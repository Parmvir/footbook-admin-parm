$(window).on('load', function() {

  var undoManager = new UndoManager();
  var productDetails = {};
  productDetails.items = {};
  productDetails.properties = {};
  productDetails.items.accessories = new Array();
  var slipperDetails = {};
  slipperDetails.items = {};
  slipperDetails.properties = {};

  // Initialize values that are pre-loaded
  productDetails.properties.size_cat = "Male";
  productDetails.items.strap_style = 17;
  productDetails.properties.strap_color = "#0A4436";
  productDetails.properties.sole_color = "#FD6B35";
  slipperDetails.items.strap_style = {id: 17,item_name: "men3c",price: 0};
  slipperDetails.items.print_design = {id: 0,item_name: "none",price: 0};
  // end Initialization
  var accessories = new Array;
  // Show page on-load only
  setTimeout(function() {
    $('body').removeClass('hide');
  }, 500);

  $('.page-start a').on('click', function() {
    $("body").css('background-color','#c7571d');
    $('body').prepend('<div class="transition-to-black" />');
    setTimeout(function() {
      var $el = $('.transition-to-black').addClass('active');
      setTimeout(function() {
        $('.page-start').hide();
        $el.addClass('unactive');
      }, 500);
      setTimeout(function() {
        $el.removeClass('active unactive');
        $('.transition-to-black').remove();
        $('.page-gallery').hide();
      }, 750);
    }, 200);
  });
  /* OLD FUNCTION */
  /*$('.page-home .btn-create-your-own').on('click', function() {
    $(this).addClass('animate');
    $('.page-home').addClass('animate');
    $('.page-choose-theme').fadeOut(); // show create slippers
    $('.page-home').slideDown("slow");
  });*/

  /* Mew Function */
  $('.page-home .btn-create-your-own').on('click', function() {
    $(this).addClass('animate');
    // $('.page-home').addClass('animate');
    $('.page-choose-theme').fadeOut(); // show create slippers
    $('.page-home').slideDown("slow");
    $('.page-gender-choices').show();
    $('.page-gender-choices').css('z-index', 900);
  });


  $('.page-gender-choices .btn-create-your-own').on('click', function() {
    // alert("i am clicked.");
    $("body").css('background-color','#f5d078');
    $(this).addClass('animate');
    $('.page-gender-choices').addClass('animate');
    $('.page-choose-theme').fadeOut(); // show create slippers
    $('.page-gender-choices').hide();
    $('.page-home').removeClass('active');
    $('.page-home').hide();
    var gender = $(this).data("gender");
    productDetails.properties.size_cat = gender;
    $('.options-item').each(function() {
      $(this).hide(); // hide all under options-item class.
    });
    $($(this).data('open')).show();
  });

  $('.page-home .home-gallery .btn-gallery').on('click', function() {
    $('.page-gallery').html("<image src='css/lib/AjaxLoader.gif'>");
    $.post('/page/view/gallery', {}, function(o){
      $('.page-gallery').html(o);
      $('.page-gallery').show();
      $('.page-gallery').css('z-index', 900);
    });
  });

  $(".btn-home,.btn-back").on('click', function(){
    $("body").css('background-color','#c7571d');
    $('.page-home').addClass('active');
    $('.page-home').removeClass('animate');
    $('.btn-green.btn-create-your-own').removeClass('animate');
    $('.page-gallery').hide();
    $('.page-home').show();
  });
  $('.options-style-color li a').each(function() {
    $(this).css('background-color', $(this).data('color'));
  });


  // $('.js-carousel').owlCarousel({
  //   items: 4,
  //   navigation: true,
  //   pagination: false,
  //   responsive: false,

  // });

  $(document).ready(function() {
 
  $(".js-carousel").owlCarousel({
 
    items: 4,
    nav: true,
    navText : ["<",">"],
    pagination: false,
    responsive: false
 
  });
 
});

  $('.js-gallery-choices').owlCarousel({
    items: 2,
    navigation: true,
    pagination: false,
    responsive: true,
    controls: true,
  });

  var themeChoicesSlider = $('.theme-choices').bxSlider({
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 400,
    pager: true,
    controls: false,
  });

  $('.page-choose-theme .bx-next').on('click', function() {
    themeChoicesSlider.goToNextSlide();
  });
  $('.page-choose-theme .bx-prev').on('click', function() {
    themeChoicesSlider.goToPrevSlide();
  });

  $('.page-choose-theme li .btn').on('click', function() {
    $('.page-choose-theme').fadeOut();
  });

  $('.options-header li a').on('click', function() {
    $('.options-item').each(function() {
      $(this).hide(); // hide all under options-item class.
    });

    $('.options-header li a').each(function() {
      $(this).removeClass('active'); // remove state of the button above.
    });

    $(this).addClass('active'); // make the button above looked clicked.

    $($(this).data('open')).show();
  });

  $('.options-size-male ul li a,.options-size-female ul li a, .js-carousel li a, .options-style-color li a').on('click', function() {
    $(this).parents('ul').find('li').each(function() {
      $(this).removeClass('active');
    });

    $(this).parent().addClass('active');
  });


  var pattern = $('.pattern').svg({
    loadURL: 'assets/slippers/pattern.svg'
  });

  function removeSelectedSlipper() {
    $('#slippers-container', svg.root()).removeAttr('class');
    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
  }

  var slippers = $('.main-show-top').svg({
    loadURL: 'assets/slippers/slippers.svg', // load ung saved svg data.
    onLoad: function() {
      var x = slippers.svg('get');

      $(svg.root()).attr('height', '500')

      $('#left, #right', svg.root()).on('dblclick', function(e) {
        $('#slippers-container', svg.root()).attr('class', 'has-active');
        $('#left, #right', svg.root()).each(function() {
          $(this).removeAttr('class');
        });
        $(this).attr('class', 'active');

        e.stopPropagation();
      });

      $(svg.root()).on('dblclick', function() {
        removeSelectedSlipper();
      });
      var defs = svg.defs();
      var filter = svg.filter(defs, 'MultiplyFilter');
      svg.filters.image(filter, 'strap-left', '#strap-left');
      svg.filters.offset(filter, 'strap-left', 'strap-left', -4, -62);
      svg.filters.blend(filter, 'blended', 'multiply', 'SourceGraphic', 'strap-left');

    }
  });

  var slippersLeft = $('.main-show-left').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $(slippersLeftSvg.root()).attr('height', 100);
      $(slippersLeftSvg.root()).attr('style', 'margin-top: 150px');
    }
  });

  var slippersLeftSvg = slippersLeft.svg('get');

  var slippersRight = $('.main-show-right').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $(slippersRightSvg.root()).attr('height', 100);
      $(slippersRightSvg.root()).attr('style', 'margin-top: 150px');
      $('g', slippersRightSvg.root()).attr('transform', 'scale(-1, 1) translate(-612.4, 0)');
    }
  });

  var slippersRightSvg = slippersRight.svg('get');

  var slippersPreviewLeft = $('.main-view-left').svg({
    loadURL: 'assets/slippers/preview-left.svg'
  });

  var slippersPreviewLeftSvg = slippersPreviewLeft.svg('get');

  var slippersPreviewRight = $('.main-view-right').svg({
    loadURL: 'assets/slippers/preview-left.svg',
    onLoad: function() {
      $('g', slippersPreviewRightSvg.root()).attr('transform', 'scale(-1, 1) translate(-612.4, 0)');
    }
  });

  var slippersPreviewRightSvg = slippersPreviewRight.svg('get');

  var slippersPreviewTop = $('.main-view-top').svg({
    loadURL: 'assets/slippers/preview-top.svg'
  });

  var slippersPreviewTopSvg = slippersPreviewTop.svg('get');

  slippers.wrapInner('<div class="panzoom"/>');
  slippersLeft.wrapInner('<div class="panzoom"/>');
  slippersRight.wrapInner('<div class="panzoom"/>');
  var panzoom = $('.panzoom').panzoom({
    $zoomIn: $('.btn-zoom-in'),
    $zoomOut: $('.btn-zoom-out'),
    increment: 0.1,
    $reset: $('.main-tools-zoom p'),
    onReset: function() {
      $('.main-tools-zoom p').text(100 + '%');
      currentZoom = 1;
    }

  });


  $('.options-size ul:eq(0) li').on('click', function() {
    switch($(this).text()) {
      case 'Male':
        modify.width(1,1);
        return;
      case 'Female':
        modify.width(0.96,1);
        return;
      case 'Kids':
        modify.width(0.93,0.93);
        return;
    }
  });

 /* $('.options-size ul:eq(1) li').on('click', function() {
    switch($(this).text()) {
      case '6':
        modify.size(1);
        return;
      case '6.5':
        modify.size(1.005);
        return;
      case '7':
        modify.size(1.010);
        return;
      case '7.5':
        modify.size(1.015);
        return;
      case '8':
        modify.size(1.020);
        return;
      case '8.5':
        modify.size(1.025);
        return;
      case '9':
        modify.size(1.030);
        return;
      case '9.5':
        modify.size(1.035);
        return;
      case '10':
        modify.size(1.04);
        return;
      case '10.5':
        modify.size(1.045);
        return;
      case '11':
        modify.size(1.050);
        return;
      case '11.5':
        modify.size(1.055);
        return;
      case '12':
        modify.size(1.060);
        return;
    }
  });*/


  // Strap style
  $(' .options-wrapper-male .my-style-strap').on('click', function() {
    $('.options-wrapper-male .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-male .options-style-color').eq(0).addClass('active');
  });

  $(' .options-wrapper-female .my-style-strap').on('click', function() {
    $('.options-wrapper-female .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-female .options-style-color').eq(0).addClass('active');
  });

    $(' .options-wrapper-male-kids .my-style-strap').on('click', function() {
    $('.options-wrapper-male-kids .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-male-kids .options-style-color').eq(0).addClass('active');
  });

  $(' .options-wrapper-female-kids .my-style-strap').on('click', function() {
    $('.options-wrapper-female-kids .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-female-kids .options-style-color').eq(0).addClass('active');
  });



  /*$('.options-style ul:eq(0) li').on('click', function() {
    $('.options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-style-color').eq(0).addClass('active');
  });*/

$('.my-style-strap').on('click', function() {
    var colorname = $(".colortest.active>a").data('colorname');
    var basename = $(this).data('basename');
    
    $(".colortest a").each(function(){
      $(this).data("basename", basename);
    });
    // alert(basename);
    $('#strap-left-container image, #strap-right-container image').each(function() {
      // basename = $(this).data('basename');


      if (!basename) {
        // basename = 'ladies3'; //default
        basename = $(".options-style-strap li .active a").data('basename');
      }
      
      $(this).data('colorname', colorname);
      // alert('assets/straps/' + basename + '-top-' + colorname+'.png');
       // $(this).attr('xlink:href', 'assets/straps/' + basename + '-top-' + colorname + '.png');
       /* save in a variable ung file */

       /* all references of xlink:href must be BASE64 */
        var top_strap = 'assets/slippers/../straps/' + basename + '-top-' + colorname + '.png';
        var side_strap = 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png';
        base64Converter(top_strap, function(data){
          $('#strap-left-container image, #strap-right-container image').attr('xlink:href', 'data:image/png;base64,'+data);
          // $(this).attr('xlink:href', 'data:image/png;base64,'+data);
        });
        base64Converter(side_strap, function(data){
          $('#strap', slippersLeftSvg.root()).attr('xlink:href', 'data:image/png;base64,'+data);
          $('#strap', slippersRightSvg.root()).attr('xlink:href', 'data:image/png;base64,'+data);
        });
      
    });

    // $('#strap', slippersLeftSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png');
    // $('#strap', slippersRightSvg.root()).attr('xlink:href', 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png');
  });

// $('.options-style-color:eq(0) li a').on('click', function() {

$('.colortest a').on('click', function() {
    var colorname = $(this).data('colorname');
    var basename = $(this).data('basename');
    
    // alert(basename);
    $('#strap-left-container image, #strap-right-container image').each(function() {
      // basename = $(this).data('basename');

      /*alert($(this));
      console.log($(this));*/

      /*if (!basename) {
        // basename = 'ladies3'; //default
        basename = $(".options-style-strap li .active a").data('basename');
      }*/
      // alert(basename);
        var top_strap = 'assets/slippers/../straps/' + basename + '-top-' + colorname + '.png';
        var side_strap = 'assets/slippers/../straps/' + basename + '-side-' + colorname + '.png';
        $(this).data('colorname', colorname);
        base64Converter(top_strap, function(data){
          $('#strap-left-container image, #strap-right-container image').attr('xlink:href', 'data:image/png;base64,'+data);
        });
        base64Converter(side_strap, function(data){
          $('#strap', slippersLeftSvg.root()).attr('xlink:href', 'data:image/png;base64,'+data);
          $('#strap', slippersRightSvg.root()).attr('xlink:href', 'data:image/png;base64,'+data);
        });
    });

    
  });

  $('.options-wrapper-female .options-style-color:eq(0) li a').on('click', function() {
    var color = $(this).data('color');
    modify.strapColor(color);
  });

  $('.options-wrapper-male .options-style-color:eq(0) li a').on('click', function() {
    var color = $(this).data('color');
    modify.strapColor(color);
  });

  // Sole style
  $('.options-wrapper-female .sole-style').on('click', function() {
    $('.options-wrapper-female .options-style-color').each(function() { $(this).removeClass('active') });
    $(' .options-wrapper-female .options-style-color').eq(1).addClass('active');
  });

  $('.options-wrapper-male .sole-style').on('click', function() {
    $('.options-wrapper-male .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-male .options-style-color').eq(1).addClass('active');
  });

  $('.options-wrapper-female-kids .sole-style').on('click', function() {
    $('.options-wrapper-female-kids .options-style-color').each(function() { $(this).removeClass('active') });
    $(' .options-wrapper-female-kids .options-style-color').eq(1).addClass('active');
  });

  $('.options-wrapper-male-kids .sole-style').on('click', function() {
    $('.options-wrapper-male-kids .options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-wrapper-male-kids .options-style-color').eq(1).addClass('active');
  });



  /*$('.options-style ul:eq(1) li').on('click', function() {
    $('.options-style-color').each(function() { $(this).removeClass('active') });
    $('.options-style-color').eq(1).addClass('active');
  });*/

  $('.options-wrapper-male .options-style-color:eq(1) li a').on('click', function() {
    var color = $(this).data('color');
    modify.soleColor(color);
  });


  $('.options-wrapper-female .options-style-color:eq(1) li a').on('click', function() {
    var color = $(this).data('color');
    modify.soleColor(color);
  });

  $('.options-wrapper-male-kids .options-style-color:eq(1) li a').on('click', function() {
    var color = $(this).data('color');
    modify.soleColor(color);
  });


  $('.options-wrapper-female-kids .options-style-color:eq(1) li a').on('click', function() {
    var color = $(this).data('color');
    modify.soleColor(color);
  });


  $('.options-design-print-design li a').each(function() {
    var image = $(this).data('image');
    $(this).parent().css('background-image', 'url(' + image + ')');

    $(this).on('click', function() {
      var imageObj = new Image();
      imageObj.onload = function() {
        var width = this.width;
        var height = this.height;

        if ($('.active', svg.root()).length) {
          var active = $('.active', svg.root());
          var x = active.attr('id');
          $('#printdesign-' + x +' image, #pattern-upper-design-'+ x + ' image').each(function() { $(this).remove(); });

          $('#printdesign-' + x +', #pattern-upper-design-'+ x + ' image').each(function() { $(this).attr('height', height).attr('width', width) });
          base64Converter(image, function(data){

            /* FOR CLEAN UP */
            svg.image($('#printdesign-' + x), 0, 0, width, height, 'data:image/png;base64,'+data);


            if (x == 'left') {
              $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
              $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');

              $('#bottom_left', svg.root()).attr('fill', 'url("#pattern-upper-design-left")');
            }

            if (x == 'right') {
              $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
              $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');

              $('#bottom_right', svg.root()).attr('fill', 'url("#pattern-upper-design-right")');
            }

            /* LEFT */
           /* $("#pattern-upper-design-left image").attr('xlink:href', 'data:image/png;base64,'+data);
            $("#pattern-upper-design-left image").attr('width', width);
            $("#pattern-upper-design-left image").attr('height', height);

            $("#pattern-upper-design-left").attr('width', width);
            $("#pattern-upper-design-left").attr('height', height);

             RIGHT 
            $("#pattern-upper-design-right image").attr('xlink:href', 'data:image/png;base64,'+data);
            $("#pattern-upper-design-right image").attr('width', width);
            $("#pattern-upper-design-right image").attr('height', height);

            $("#pattern-upper-design-right").attr('width', width);
            $("#pattern-upper-design-right").attr('height', height);*/
          });
          

        }

        else {
          // alert("I get in here");
          $('#printdesign image, #printdesign-left image, #printdesign-right image').each(function() { $(this).remove(); });
          $('#printdesign, #printdesign-left, #printdesign-right').each(function() { $(this).attr('height', height).attr('width', width) });
          base64Converter(image, function(data){

            /* LEFT */
            $("#pattern-upper-design-left image").attr('xlink:href', 'data:image/png;base64,'+data);
            $("#pattern-upper-design-left image").attr('width', width);
            $("#pattern-upper-design-left image").attr('height', height);

            $("#pattern-upper-design-left").attr('width', width);
            $("#pattern-upper-design-left").attr('height', height);

            /* RIGHT */
            $("#pattern-upper-design-right image").attr('xlink:href', 'data:image/png;base64,'+data);
            $("#pattern-upper-design-right image").attr('width', width);
            $("#pattern-upper-design-right image").attr('height', height);

            $("#pattern-upper-design-right").attr('width', width);
            $("#pattern-upper-design-right").attr('height', height);
          });


          svg.image($('#printdesign-left'), 0, 0, width, height, image);
          svg.image($('#printdesign-right'), 0, 0, width, height, image);
          svg.image($('#printdesign'), 0, 0, width, height, image);
          $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
          $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
          $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
          $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
        }



      };

      imageObj.src = image;

    });
  });

  $('.btn-mirror').on('click', function() {
    /* Pseudo code

    Get active slipper

    Get active slipper pattern

    Copy image on  opposite slipper pattern

    Copy attributes (height width transform)

    Perform transform to mirror

    Set opposite slipper transformed pattern

     */

    if ($('.active', svg.root()).length) {
      var active = $('.active', svg.root());
      var x = active.attr('id');

      if (x == 'left' || x == 'right') {

        $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
        $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
        $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
        $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');

        var patternSource;
        var patternDestination;

        if (x == 'left') {
          patternSource = $('#printdesign-left');
          patternDestination = $('#printdesign-right');
        }
        else if (x == 'right') {
          patternSource = $('#printdesign-right');
          patternDestination = $('#printdesign-left');
        }

        var pattern = patternSource.find('image').clone();

        patternDestination.find('image').remove();

        var w = patternSource.attr('width');
        var h = patternSource.attr('height');
        var px = patternSource.attr('x');
        var py = patternSource.attr('y');
        var pr = patternSource[0].getAttribute("patternTransform");

        patternDestination.attr('x', -(parseInt(px)) + 450);
        patternDestination.attr('y', py);
        patternDestination.attr('width', w);
        patternDestination.attr('height', h);
        if (pr) { patternDestination[0].setAttribute('patternTransform', pr); }

        $(pattern).attr('transform', "scale(-1,1) translate(-" + w + ",0)");

        $(pattern).appendTo(patternDestination);

      }

    }

    else {
      alert('Please select a slipper');
    }

  });

  $('.btn-remove-upload').on('click', function() {
  });

  var currentZoom = 1;

  $('.btn-zoom-out').on('click', function() {
    currentZoom -= 0.1;
    modify.zoom(currentZoom);
  });

  $('.btn-zoom-in').on('click', function() {
    currentZoom += 0.1;
    modify.zoom(currentZoom);
  });

  $('#upload').click(function(e) {
    e.stopPropagation();
  });

  $('.btn-upload').on('click', function() {
    $('#upload').click();
  });

  window.previewImage = function(img) {
    if (img.files && img.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        var image = new Image();
        image.onload = function () {

          var width = this.width;
          var height = this.height;

          var image = e.target.result;


          if ($('.active', svg.root()).length) {
            var active = $('.active', svg.root());
            var x = active.attr('id');
            $('#printdesign-' + x +' image').each(function() { $(this).remove(); });

            $('#printdesign-' + x).each(function() { $(this).attr('height', height).attr('width', width) });

            svg.image($('#printdesign-' + x), 0, 0, width, height, image);


            if (x == 'left') {
              $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
              $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
            }

            if (x == 'right') {
              $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
              $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
            }

          }

          else {
            $('#printdesign image, #printdesign-left image, #printdesign-right image').each(function() { $(this).remove(); });
            $('#printdesign, #printdesign-left, #printdesign-right').each(function() { $(this).attr('height', height).attr('width', width) });
            svg.image($('#printdesign-left'), 0, 0, width, height, image);
            svg.image($('#printdesign-right'), 0, 0, width, height, image);
            svg.image($('#printdesign'), 0, 0, width, height, image);
            $('#front_left', svg.root()).attr('fill', 'url("#printdesign-left")');
            $('#front_right', svg.root()).attr('fill', 'url("#printdesign-right")');
            $('#preview-left-front', slippersLeftSvg.root()).attr('fill', 'url("#printdesign-left")');
            $('#preview-left-front', slippersRightSvg.root()).attr('fill', 'url("#printdesign-right")');
          }

          //$('#printdesign image', svg.root()).remove();
          //
          //svg.image($('#printdesign', svg.root()), 0, 0, width, height, e.target.result);
          //
          //$('#printdesign', svg.root()).attr('height', height).attr('width', width);
          //
          //$('.front', svg.root()).attr('fill', 'url("#printdesign")');
          //
          //$('.options-design-upload-tools').slideDown();

        }
        image.src = e.target.result;
      }
      reader.readAsDataURL(img.files[0]);
    }
  }



  var svg = slippers.svg('get');

  var modify = {
    width: function(x, y) {
      $('#left, #right', svg.root()).attr('transform', 'scale(' + x + ',' + y +')');

      undoManager.add({
        undo: function() {
          //$('#left, #right', svg.root()).attr('transform', 'scale(' + x + ',' + y +')');
          $('#left, #right', svg.root()).attr('transform', '');
        },
        redo: function() {
          $('#left, #right', svg.root()).attr('transform', 'scale(' + x + ',' + y +')');
        }
      });

    },

    size: function(x) {
      $('#bottom_left, #front_left, #bottom_right, #front_right', svg.root()).attr('transform', 'scale(1, ' + x + ')');

    },
    zoom: function(x) {
      //$('#slippers-container', svg.root()).attr('transform', 'scale(' + x + ',' + x + ')' );
      $('.main-tools-zoom p').text(Math.floor(currentZoom * 100) + '%');

    },

    soleColor: function(x) {
      $('#front_left, #front_right', svg.root()).each(function() {
        $(this).attr('fill', x);
      });
      /*$('#bottom_left, #bottom_right', svg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });*/

      // Main Right
      $('#preview-left-front', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Main Left
      $('#preview-left-front', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Right
      $('#preview-left-front', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Left
      $('#preview-left-front', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-bottom', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });

      // Preview Top
      $('#preview-top-sole', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-top-sole-bottom', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -25));
      });
    },

    strapColor: function(x) {
      $('#strap-left, #strap-right', svg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#strap-highlight-right path, #strap-connector-right, #strap-highlight-left path, #strap-connector-left', svg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Main Right
      $('#preview-left-strap-front', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Main Left
      $('#preview-left-strap-front', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Right
      $('#preview-left-strap-front', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersPreviewRightSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Left
      $('#preview-left-strap-front', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-left-strap-bottom, #preview-left-strap-connector', slippersPreviewLeftSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });

      // Preview Top
      $('#preview-top-strap', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', x);
      });
      $('#preview-top-strap-shadow path', slippersPreviewTopSvg.root()).each(function() {
        $(this).attr('fill', shadeColor1(x, -15));
      });
    },

    uploadZoom: function(x) {
      if ($('.active', svg.root()).length) {
        var active = $('.active', svg.root());
        var activeId = active.attr('id');
        $('#printdesign-' + activeId).attr('patternTransform', 'scale(' + x + ',' + x + ')' );
        $('##pattern-upper-design-' + activeId + ' image').attr('patternTransform', 'scale(' + x + ',' + x + ')' );
      }
      else {
        $('#printdesign-left, #printdesign-right, #pattern-upper-design-right image, #pattern-upper-design-left image').attr('patternTransform', 'scale(' + x + ',' + x + ')' );
      }
    },

    uploadMove: function(direction) {
      if ($('.active', svg.root()).length) {
        var active = $('.active', svg.root());
        var x = active.attr('id');
        var currentX = parseInt($('#printdesign-' + x).attr('x'));
        var currentY = parseInt($('#printdesign-' + x).attr('y'));
        if (direction == 'up') {
          $('#printdesign-' + x).attr('y', currentY - 20 );
          $('#pattern-upper-design-' + x).attr('y', currentY - 20 ); // Added by John
        }
        if (direction == 'down') {
          $('#printdesign-' + x).attr('y', currentY + 20 );
          $('#pattern-upper-design-' + x).attr('y', currentY + 20 ); // Added by John
        }
        if (direction == 'left') {
          $('#printdesign-' + x).attr('x', currentX - 20 );
          $('#pattern-upper-design-' + x).attr('x', currentX - 20 ); // Added by John
        }
        if (direction == 'right') {
          $('#printdesign-' + x).attr('x', currentX + 20 );
          $('#pattern-upper-design-' + x).attr('x', currentX + 20 ); // Added by John
        }
      }
      else {
        var currentX = parseInt($('#printdesign-left, #printdesign-right').attr('x'));
        var currentY = parseInt($('#printdesign-left, #printdesign-right').attr('y'));
        if (direction == 'up') {
          $('#printdesign-left, #printdesign-right, #pattern-upper-design-left image, #pattern-upper-design-right image').attr('y', currentY - 20 );
        }
        if (direction == 'down') {
          $('#printdesign-left, #printdesign-right, #pattern-upper-design-left image, #pattern-upper-design-right image').attr('y', currentY + 20 );
        }
        if (direction == 'left') {
          $('#printdesign-left, #printdesign-right, #pattern-upper-design-left image, #pattern-upper-design-right image').attr('x', currentX - 20 );
        }
        if (direction == 'right') {
          $('#printdesign-left, #printdesign-right, #pattern-upper-design-left image, #pattern-upper-design-right image').attr('x', currentX + 20 );
        }
      }
    }

  }

  var uploadZoomValue = 1;

  $('.btn-upload-zoom-out').on('click', function() {
    uploadZoomValue -= 0.1;
    modify.uploadZoom(uploadZoomValue);
  });

  $('.btn-upload-zoom-in').on('click', function() {
    uploadZoomValue += 0.1;
    modify.uploadZoom(uploadZoomValue);
  });

  $('.btn-upload').on('click', function() {
    $(this).next().click();
  });
  $('.btn-upload-move-up').on('click', function() {
    modify.uploadMove('up');
  });
  $('.btn-upload-move-down').on('click', function() {
    modify.uploadMove('down');
  });
  $('.btn-upload-move-left').on('click', function() {
    modify.uploadMove('left');
  });
  $('.btn-upload-move-right').on('click', function() {
    modify.uploadMove('right');
  });

  function shadeColor1(color, percent) {
    var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
  }


  $('.btn-close, .btn-skip, .btn-done, .js-close-popup').on('click', function() {
    $(this).parent().removeClass('active');
  });

  $('.btn-login').on('click', function() {
    $('.popup-signin').addClass('active');
  });
  $('.btn-checkout').on('click', function() {
    $('.popup-checkout').addClass('active');

    $("#left, #right",svg.root()).removeClass("active");
    $(".main-view-left,.main-view-right,.main-view-top").removeClass("active");
    $(".main-view-top").click();
    $(".popup-checkout-left .center .image-container").html($(".main-show-top").eq(0).clone());

    $.post("/checkout/gallery/selection", {}, function(o){
      $(".my-creation-list").html(o);
      $.post("/checkout/summary", slipperDetails, function(o){
        sessionStorage.setItem('slipperDetails', JSON.stringify(slipperDetails));
        $(".summary-details-checkout").html(o);
      });
    });

    
  });
  
  $('.btn-logout').on('click', function(){
    $('.popup-logout').addClass('active');
  });

  $('.btn-no-logout').on('click', function(){
    $('.popup-logout').removeClass('active');
  });

  $('.btn-register').on('click', function(){
    $('.popup-account-register').addClass('active');
  });

  $('.btn-save-preview').on('click', function(){
    $(".popup-save-preview").addClass('active');
    $("#left, #right",svg.root()).removeClass("active");
    $(".main-view-left,.main-view-right,.main-view-top").removeClass("active");
    $(".main-view-top").click();
    $(".popup-save-preview .center .image-container").html($(".main-show-top").eq(0).clone());
    $.post('/preview/save',{}, function(o){
      $('.popup-preview-designs').html(o);
    });
  });
  // $('.btn-checkout-final').on('click', function() {
  //   $('.popup-checkout').removeClass('active');
  //   $('.popup-get-ticket').addClass('active');
  // });

  /*$('.options-style-strap li a').on('click', function() {
    $('#strap-pattern-left, #strap-pattern-right', svg.root()).remove();
    if ($(this).find('img').attr('src') != 'assets/straps/strap-00.svg') {
      svg.image($('#strap-left-pattern', svg.root()), 21, 79, 171, 171, $(this).find('img').attr('src'), {
        id: 'strap-pattern-left',
        filter: 'url(#MultiplyFilter)',
        opacity: 0.8
      });
      svg.image($('#strap-right-pattern', svg.root()), 21, 79, 171, 171, $(this).find('img').attr('src'), {
        id: 'strap-pattern-right',
        transform: 'scale(-1, 1) translate(-431, 0)',
        filter: 'url(#MultiplyFilter)',
        opacity: 0.8
      });
    }
  });*/


  function hideShows() {
    $('.main-show > div').each(function() { $(this).hide(); });
    $('.main-view > div').each(function() { $(this).removeClass('active') });
  };

  $('.main-show-top').show();

  $('.main-view-left').on('click', function() {
    hideShows()
    $('.main-show-left').show();
    $(this).addClass('active');


    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
    $('#left', svg.root()).attr('class', 'active');
  });
  $('.main-view-top').on('click', function() {
    hideShows()
    $('.main-show-top').show();
    $(this).addClass('active');
    removeSelectedSlipper();
  });
  $('.main-view-right').on('click', function() {
    hideShows()
    $('.main-show-right').show();
    $(this).addClass('active');
    $('#left, #right', svg.root()).each(function() {
      $(this).removeAttr('class');
    });
    $('#right', svg.root()).attr('class', 'active');
  });

  // $('.js-keyboard').onScreenKeyboard();


  $('.options-strap-accessories li a').on('click', function() {
    var img = $(this).find('img').attr('src');
    var price = $(this).data('price');
    $('.popup-accessory div img').attr('src', img);
    $('.popup-accessory .currency strong').text(price);
    $("#accessory_done").data("itemId", $(this).data('itemId'));
    $("#accessory_done").data("property", $(this).data('property'));
    $("#accessory_done").data("price", price);
    $('.popup-accessory').addClass('active');
  });


  var accessory = null;

  var currentAccessory;

  $('.js-add-accessory').on('click', function() {

    /*
    Get svg root
    get image source
    add to left slipper
    disable panzoom
    make image draggable
    limit image draggable
     */

    $(this).parents('.popup').removeClass('active');

    $('.main-place-accessory.cover').addClass('active');

    $('.main-view-top').click();

    var imageSource = $('.popup-accessory').find('img').attr('src');
    // accessory = svg.image($('#left', svg.root()), 13, 155, 40, 40, imageSource);
    base64Converter(imageSource, function(data){
      accessory = svg.image($('#left', svg.root()), 13, 155, 40, 40,'data:image/png;base64,'+data);
      var path = $('#strap-left-path', svg.root())[0];
      $(panzoom).each(function() {
        $(this).panzoom('reset');
        $(this).panzoom('disable');
      });

      var gradSearch = function (l0, pt) {
        l0 = l0 + totLen;
        var l1 = l0,
          dist0 = dist(path.getPointAtLength(l0 % totLen), pt),
          dist1,
          searchDir;

        if (dist(path.getPointAtLength((l0 - searchDl) % totLen), pt) >
          dist(path.getPointAtLength((l0 + searchDl) % totLen), pt)) {
          searchDir = searchDl;
        } else {
          searchDir = -searchDl;
        }

        l1 += searchDir;
        dist1 = dist(path.getPointAtLength(l1 % totLen), pt);
        while (dist1 < dist0) {
          dist0 = dist1;
          l1 += searchDir;
          dist1 = dist(path.getPointAtLength(l1 % totLen), pt);
        }
        l1 -= searchDir;
        return (l1 % totLen);
      };

      var dist = function (pt1, pt2) {
        var dx = pt1.x - pt2.x;
        var dy = pt1.y - pt2.y;
        return Math.sqrt(dx * dx + dy * dy);
      };

      var l = 0;
      var searchDl = 1;
      var totLen = path.getTotalLength();

      $(accessory).draggable()
        .bind('drag', function(event, ui) {

          var mouseX = event.clientX - $('.main-show-top').offset().left;
          var container = $('.main-show-top').width();

          if (mouseX < container/2) {
            path = $('#strap-left-path', svg.root())[0];
            $(accessory).appendTo('#left');
            // base64Converter(imageSource, function(data){
            //   var my_accessory = '<image xmlns:xlink="http://www.w3.org/1999/xlink" '
            //   svg.image($('#left', svg.root()), 13, 155, 40, 40,'data:image/png;base64,'+data);
            // });
          }

          else {
            path = $('#strap-right-path', svg.root())[0];
            $(accessory).appendTo('#right');
          }

          var $this = accessory;
          var whichDrag = $this;
          var tmpPt = {
            x : event.clientX - $('.main-show-top').offset().left - 150,
            y : event.clientY - $('.main-show-top').offset().top - 50
          };

          l = gradSearch(l, tmpPt);
          pt = path.getPointAtLength(l);

          //$(accessory).attr({x: pt.x, y: pt.y});
          $(accessory).attr({x: pt.x - 20, y: pt.y - 20});

        })
        .on('mouseover', function() {
          var x = $(this).offset().top;
          var y = $(this).offset().left;
          $('.accessory-close').css({top: x, left: y}).addClass('active');

          currentAccessory = $(this);

        })
        .on('mouseout', function() {
          $('.accessory-close').removeClass('active');
        });
    });
    // var accessorySrc = localStorage.getItem('accessorySrc');
    
  });

  $('.accessory-close').on('click', function() {
    $(currentAccessory).remove();
    removeAccessory(parseInt($(currentAccessory).data('id')));
    $(this).removeClass('active');

  });
  $('.accessory-close').on('mouseover', function() {
    $(this).addClass('active');

  });
  $('.main-place-accessory .btn-cancel').on('click', function() {
    $(panzoom).each(function() {
      $(this).panzoom('enable');
    });
    $('.main-place-accessory.cover').removeClass('active');
    $(accessory).remove();

  });
  $('.main-place-accessory .btn-done').on('click', function() {
    $(panzoom).each(function() {
      $(this).panzoom('enable');
    });
    $('.main-place-accessory.cover').removeClass('active');
    $(accessory).draggable('destroy').off('drag');
    $(accessory).attr('data-id',$(this).data('itemId'));
    accessories.push({id: $(this).data('itemId'),item_name: $(this).data('property'),price: $(this).data('price')});
    slipperDetails.items.accessories = accessories;
    productDetails.items.accessories.push($(this).data('itemId'));
    console.log(productDetails.items.accessories);
    computeTotalPrice();
  });

  $('.btn-undo').on('click', function() {
    undoManager.undo();
    console.log('undo');
  });

  $('.btn-redo').on('click', function() {
    undoManager.redo();
  });

  $('.theme-choices .btn').on('click', function() {
    $('body').addClass($(this).data('theme'));


    // Add background-----------

   

    $('.page-creator').prepend('<div class="page-creator-bg"/>');
    $('body').append('<style>.page-creator-bg {position: absolute; height: 100%; width: 100%; background-size: cover; z-index: 10; opacity: 0.3; margin-top: -1em; margin-left: -1em;' + $(this).prev().attr('style') + '}</style>')
    $('.page-creator .header, .page-creator .body').css('z-index', 20).css('position', 'relative');
  });

$('.theme-choices .grey').on('click', function() {
    $('body').addClass($(this).data('theme'));


    // Add background-----------

   
    $('.page-creator').prepend('<div class="page-creator-bg"/>');
     $('body').append('<style>.page-creator-bg {background:#5C5B5B !important; position: absolute; height: 100%; width: 100%; background-size: cover; z-index: 10; opacity: 0.3; margin-top: -1em; margin-left: -1em;' + $(this).prev().attr('style') + '}</style>')
    $('.page-creator .header, .page-creator .body').css('z-index', 20).css('position', 'relative');
  });

  /*
   * Set details
   *
   */

  /*$('#size_cat a').click(function() {
    productDetails.properties.size_cat = $(this).text();
    console.log(productDetails);
    computeTotalPrice();
  });*/

  $('#size a').click(function() {
    productDetails.properties.size = $(this).text();
    console.log(productDetails);
    computeTotalPrice();
  });

  $('#strap_style a').click(function() {
    productDetails.items.strap_style = $(this).data('itemId');
    slipperDetails.items.strap_style = {id: $(this).data('itemId'),item_name: $(this).data('property'),price: $(this).data('price')};
    console.log(productDetails);
    computeTotalPrice();
  });

  $('#strap_color a').click(function() {
    productDetails.properties.strap_color = $(this).data('color');
    console.log(productDetails);
    computeTotalPrice();
  });

  $('#sole_color a').click(function() {
    productDetails.properties.sole_color = $(this).data('color');
    console.log(productDetails);
    computeTotalPrice();
  });

  $('#print_design a').click(function() {
    productDetails.items.print_design = $(this).data('itemId');
    slipperDetails.items.print_design = {id: $(this).data('itemId'),item_name: $(this).data('property'),price: $(this).data('price')};
    console.log(productDetails);
    computeTotalPrice();
  });

  $('.btn-checkout-final').on('click', function() {
    if($("#total_qty").html() > 0){
      $.post('/auth/check/login', {}, function(data){
        if(data.is_logged_in){
          var product_id = parseInt($("#product_id").val());
          var creation_id = parseInt($("#my_creation").val());
          
          if(!isNaN(product_id) && isNaN(creation_id)){
            // CHECKOUT CURRENT PRODUCT
            checkout(product_id);
          } else if(isNaN(product_id) && !isNaN(creation_id)){
            // CHECKOUT CHOSEN PRODUCT
            checkout(creation_id);
          } else if(!isNaN(product_id) && !isNaN(creation_id)){
            // CHECKOUT WHICH ONE CURRENT OR CHOSEN
            checkout(creation_id);
          } else {
            // CHECKOUT CURRENT PRODUCT AND SAVE IT AS PRODUCT
            var title = prompt("Please Enter Product Title.");
            if(title === null){
              return;
            } else if($.trim(title) === ""){
              alertMe('Please Enter Product Title. Product not saved.', 'red');
              return;
            } else {
              productDetails.title = title;
              console.log(productDetails);
              var svg = $('#slippers')[0];
              var serializer = new XMLSerializer();
              productDetails.slippers = serializer.serializeToString(svg);

              $.post('/products/save', productDetails)
                .done(function(data) {
                  $('.popup-checkout').removeClass('active');
                  alertMe('Product - ' + title + ' has been saved.', 'green');
                  $("#product_id").val(data);
                  $("#checkout_product_id").val(data);
                  checkout(data);
                })
                .fail(function() {
                  alert('Error saving design');
                });
            }
          }
        } else {
          alert('We cannot save your work if you are not login.');
        }
      });
    } else {
      alert("Please place your desired sizes and quantity.");
    }
  });

  $('.btn-save-design').on('click', function(){
    $.post('/auth/check/login', {}, function(data){
      if(data.is_logged_in){
        var title = prompt("Please Enter Product Title.");
        if(title === null){
          return;
        } else if($.trim(title) === ""){
          alertMe('Please Enter Product Title. Product not saved.', 'red');
          return;
        } else {
          productDetails.title = title;
          console.log(productDetails);
          var svg = $('#slippers')[0];
          var serializer = new XMLSerializer();
          productDetails.slippers = serializer.serializeToString(svg);

          $.post('/products/save', productDetails)
            .done(function(data) {
              $('.popup-save-preview').removeClass('active');
              alertMe('Product - ' + title + ' has been saved.', 'green');
              $("#product_id").val(data);
              $("#checkout_product_id").val(data);
            })
            .fail(function() {
              alertMe('Error saving design','Red');
            });
        }
      } else {
        alertMe('We cannot save your work if you are not login.','Red');
      }
    });
  });

  function checkout(product_id){
    var checkout_form = $("#checkout-form").serializeArray();
    
    $.post('/ajax/checkout/save', checkout_form)
    .done(function(data) {
      $('.popup-checkout').removeClass('active');
      $('.order-ticket-number').html(data);
      $('.popup-get-ticket').addClass('active');
    })
    .fail(function() {
      alert('Error Checkout.');
    });
  }

  function removeAccessory(accessory_id){
    var index = productDetails.items.accessories.indexOf(accessory_id);
    if (index > -1) {
        productDetails.items.accessories.splice(index, 1);
    }
    $.each(slipperDetails.items.accessories, function(index,value){
      if(accessory_id == value.id){
        slipperDetails.items.accessories.splice(index, 1);
      }
    });

    computeTotalPrice();

  }

  function computeTotalPrice(){
    var pd_price = (slipperDetails.items.hasOwnProperty('print_design') ? parseFloat(slipperDetails.items.print_design.price) : 0);
    var ss_price = (slipperDetails.items.hasOwnProperty('strap_style') ? parseFloat(slipperDetails.items.strap_style.price) : 0);
    var accessory_price = 0;
    if(slipperDetails.items.hasOwnProperty('accessories')){
      // productDetails.items.accessories.forEach()
      $.each(slipperDetails.items.accessories, function(index,value){
        accessory_price += parseFloat(value.price);
        // console.log(value.price);
      });
    }
    var total_price = pd_price + ss_price + accessory_price;
    $(".main-checkout b").text(total_price + ".00");
  }



  function dataForProcess(){
    var arr = new Array;
    if(slipperDetails.items.hasOwnProperty('print_design')){
      var pd = {printdesign: {name: "Print Design", price: slipperDetails.items.print_design.price}};
      arr.push(pd);
    }
    if(slipperDetails.items.hasOwnProperty('strap_style')){
      var ss = {strap_style: {name: "Strap Style", price: slipperDetails.items.strap_style.price}};
      arr.push(ss);
    }
    if(slipperDetails.items.hasOwnProperty('accessories')){
      $.each(slipperDetails.items.accessories, function(index,value){
        var acc = {accessory: {name: value.item_name, price: value.price}};
        arr.push(acc);
      });
    }
    // return arr.serializeArray();
  }

  /* START CONVERTER */
  function base64Converter(url, callback) {
      var img = new Image();

      img.setAttribute('crossOrigin', 'anonymous');

      img.onload = function () {
          var canvas = document.createElement("canvas");
          canvas.width =this.width;
          canvas.height =this.height;

          var ctx = canvas.getContext("2d");
          ctx.drawImage(this, 0, 0);

          var dataURL = canvas.toDataURL("image/png");
          callback(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
          // alert(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
      };

      img.src = url;
  }
  /* END CONVERTER */

});

jQuery( window ).ready(function() {
  jQuery("body").css('background-color','#0078cb');

});

//wag muna tanggalinito

// jQuery( window ).ready(function() {

//     if(jQuery(window).width() > 1600) {
//        document.body.style.zoom="100%";
//     }else if(jQuery(window).width() > 1024) {
//       document.body.style.zoom="82%";
//     } else{
//       document.body.style.zoom="98%";
//     } 
// });

// jQuery( window ).resize(function() {

//     if(jQuery(window).width() > 1600) {
//        document.body.style.zoom="100%";
//     }else if(jQuery(window).width() > 1024) {
//       document.body.style.zoom="82%";
//     } else{
//       document.body.style.zoom="98%";
//     } 
// });

   

