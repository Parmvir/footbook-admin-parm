<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\LookupsValue as LookupsValue;
use Nicolaslopezj\Searchable\SearchableTrait;

class Order extends Model {

	//
	use LookupsValue;
	use SearchableTrait;
	protected $searchable = [
		'columns' => [
			'order_id' => 10,
			'amount' => 10,
			'o_status' => 10,
		]
	];

	public static function generateNewOrderNumber() {
		$order = Order::orderBy('id','desc')->first();
		$nextId = ($order ? (int) $order->id + 1 : 1);

		return $orderNumber = "ORDER-" . str_pad($nextId, 5, "0",STR_PAD_LEFT);
	}

	public function items()
	{
		return $this->hasMany('App\OrderItem');
	}

	public function scopeCounter($query){
		return $this->where('o_status', '=', 'New')->count();
	}
}
