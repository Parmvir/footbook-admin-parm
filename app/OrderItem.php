<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model {

	//
	protected $fillable = ['product_id','size','country_size','quantity','amount','total_amount']; 
	
	public function order()
	{
		return $this->belongsTo('App\Order');
	}

}
