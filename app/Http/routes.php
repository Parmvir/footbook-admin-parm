<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
// Route::get('industrial', ['uses'=>'HomeController@industrial', 'as'=>'industrial']);

Route::group(['before'=>'csrf'], function(){
	Route::post('/login', array(
			'as'=> 'login.post',
			'uses'=> 'LoginController@store'
	));
	Route::post('/ajax/checkout/save', array(
			'as'=> 'checkout.save.post',
			'uses'=> 'CheckoutController@store'
	));
	Route::post('/user/register', array(
			'as'=> 'user.register',
			'uses'=> 'AjaxController@registerUser'
	));
});


Route::post('/check/user/email', array(
		'as' => 'check.user.email',
		'uses' => 'AjaxController@checkEmailAddress'
	));

Route::post('/get/size/legend', array(
		'as' => 'size.legend',
		'uses' => 'AjaxController@getSizeLegend'
	));

Route::post('/user/popup/view', array(
		'as' => 'user.popup.view',
		'uses' => 'UsersController@viewPopupUser'
	));

Route::post('/user/popup/view/gallery', array(
		'as' => 'user.popup.view.gallery',
		'uses' => 'UsersController@viewPopupUserGallery'
	));

Route::post('/page/view/gallery', array(
		'as' => 'page.view.gallery',
		'uses' => 'AjaxController@loadPageGallery'
	));

Route::post('/footbook/selection', array(
		'as' => 'footbook.selection',
		'uses' => 'AjaxController@loadSelection'
	));

Route::post('/checkout/summary', array(
		'as' => 'checkout.summary',
		'uses' => 'AjaxController@loadCheckoutRightSide'
	));

Route::post('/preview/save', array(
		'as' => 'preview.save',
		'uses' => 'AjaxController@loadPreviewRight'
	));

Route::post('/checkout/gallery/selection', array(
		'as' => 'checkout.gallery.selection.right',
		'uses' => 'AjaxController@loadMyGallerySelectionRight'
	));

Route::post('/destroy', array(
		'as' => 'logout.frontend',
		'uses' => 'LoginController@destroy'
	));

Route::post('/auth/check/login', array(
		'as' => 'auth.check.login',
		'uses' => 'AjaxController@checkAuth'
	));

Route::group(['prefix'=>'admin'], function(){

	Route::get('/', function() {
		if (Auth::guest()) {
			return redirect()->to('/admin/auth/login');
		}
		else {
			return redirect()->route('admin.dashboard');
		}
	});
	Route::group(['middleware' => 'auth'], function(){
		Route::get('dashboard', ['uses'=>'HomeController@admin', 'as'=>'admin.dashboard']);

		Route::get('/items/search', ['uses'=>'ItemsController@search', 'as'=>'admin.items.search']);
		Route::resource('items', 'ItemsController');
		Route::get('/items/{id}/delete', ['uses'=>'ItemsController@destroy', 'as'=>'admin.items.destroy']);

		Route::resource('users', 'UsersController');
		Route::get('/users/{id}/delete', ['uses'=>'UsersController@destroy', 'as'=>'admin.users.destroy']);

		Route::get('/themes/search', ['uses'=>'ThemesController@search', 'as'=>'admin.themes.search']);
		Route::resource('themes', 'ThemesController');
		Route::get('/themes/{id}/delete', ['uses'=>'ThemesController@destroy', 'as'=>'admin.themes.destroy']);

		Route::resource('products', 'ProductsController');
		Route::get('/products/{id}/delete', ['uses'=>'ProductsController@destroy', 'as'=>'admin.products.destroy']);

		Route::get('/roles/permissions', ['uses'=>'RolesController@editPermissions', 'as'=>'admin.permissions.edit']);
		Route::post('/roles/permissions', ['uses'=>'RolesController@updatePermissions', 'as'=>'admin.permissions.update']);
		Route::resource('roles', 'RolesController');

		Route::get('/checkout/search', ['uses'=>'CheckoutController@search', 'as'=>'admin.checkout.search']);
		Route::resource('checkout', 'CheckoutController');
		Route::get('/checkout/{id}/delete', ['uses'=>'CheckoutController@destroy', 'as'=>'admin.checkout.destroy']);

	});
	

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);


});

Route::post('/products/save', ['uses'=>'ProductsController@store', 'as'=>'products.store']);
