<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\ProductProperty;
use App\Item;
use App\Theme;
use Input;
use App\User;
use App\UserInfo;
use Hash;
class AjaxController extends Controller {

	public function loadSelection(){
		if(Auth::check()){
			if(Auth::user()->roles->first()->id == 3){ // Industrial
				$items = Item::consumer(2)->get()->groupBy('type_name');
			} else {
				$items = Item::consumer(1)->get()->groupBy('type_name');
			}
		} else {
			$items = Item::consumer(1)->get()->groupBy('type_name');
		}
		return view('footbook-selection', compact('items'));
	}

	public function loadCheckoutRightSide(){
		$items = Input::all();
		if(Input::has('id')){
			$id = $items['id'];
			$product = Product::find($id)->load(['properties', 'items']);
			$total = 0;
			foreach ($product->items as $item) {
				$total += $item->price;
			}
			return view('pages.checkout-right-via-id',compact('product','total'));
		} else {
			$pd = (isset($items['items']['print_design']) ? (float) $items['items']['print_design']['price'] : 0);
			$ss = (isset($items['items']['strap_style']) ? (float) $items['items']['strap_style']['price'] : 0);
			$acc = 0;
			if(isset($items['items']['accessories'])){
				foreach ($items['items']['accessories'] as $key => $value) {
					$acc += (float) $value['price'];
				}
			}

			$total = $pd + $ss + $acc;
			return view('footbook-checkout-right', compact('items','total'));
		}
		
	}

	public function checkAuth(){
		$json['is_logged_in'] = (Auth::check() ? true : false);
		return response()->json($json);
	}

	public function loadPageGallery(){
		$products = Product::where('featured','=',1)->orderBy('created_at','desc')->get();
		if($products->count() >= 1){

			foreach ($products as $key => $value) {
				$user = User::find($value['user_id']);
				 $arr[$key] = array(
				 	"product_path" 	=> $value['product_path'],
				 	"product_id"	=> $value['id'],
				 	"username"		=> $user->first_name . " " . $user->last_name
				 	);
			}

			$data['products'] = $arr;
		} else {
			$data['products'] = $products;
		}
		return view('pages.page-gallery', $data);
	}

	public function loadMyGallerySelectionRight(){
		if(Auth::check()){
			$user_id = Auth::user()->id;
			$data['products'] = $products = Product::where('user_id','=',$user_id)->orderBy('created_at','desc')->get();
			return view('pages.my-gallery-selection-right', $data);
		} else {
			return "We have problem loading your gallery. Please refresh the page.";
		}
	}

	public function loadPreviewRight(){
		if(Auth::check()){
			$user_id = Auth::user()->id;
			$data['products'] = $products = Product::where('user_id','=',$user_id)->take(2)->orderBy('created_at','desc')->get();
			// dd($products);
			return view('pages.save-preview-right', $data);
		} else {
			return "We have problem loading your gallery. Please refresh the page.";
		}
	}

	public function checkEmailAddress(){
		$input = Input::all();

		$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		if (preg_match($pattern, $input['email_address']) === 1) {
		   $count = User::where('email', '=', $input['email_address'])->count();
			if($count){
				$json['is_duplicate'] 	= true;
				$json['message'] 		= "Email Address already taken.";
			} else {
				$json['is_duplicate'] 	= false;
				$json['message'] 		= "Email Address is available.";
			}
		} else {
			$json['is_duplicate'] 	= true;
			$json['message'] 		= "Invalid email address!";
		}
		
		return response()->json($json);
	}

	public function registerUser(Registrar $registrar, Guard $auth){
		$input = Input::all();

		$password 	= Hash::make($input['password']);
		$role = ((int) $input['role'] ? $input['role'] : 4);

		if($role != 3 || $role != 4){
			$role = 4;
		}
		$user = User::create(array(
			"email" 			=> $input['email_address'],
			"password" 			=> $password,
			"first_name" 		=> $input['first_name'],
			"middle_initial" 	=> $input['middle_initial'],
			"last_name" 		=> $input['last_name'],
			"address_1" 		=> $input['address_1'],
			"address_2" 		=> $input['address_2'],
			"contact_number" 	=> $input['contact_number']
			));
		$user->roles()->sync([$role]);

		if($user->id){
			$json['is_successful'] 	= true;
			$json['message']		= "Thank you for registering. You may now login.";
		} else {
			$json['is_successful'] 	= false;
			$json['message']		= "Register Failed. Please try again later.";
		}
		return response()->json($json);
	}

	public function getSizeLegend(){
		return view('pages.size-legend');
	}

}
