<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input, Auth;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$post = Input::all();
		// dd($post);
		if(Input::has('username') && Input::has('password')){
			$credentials = array(
				"email" 		=> $post['username'],
				"password"		=> $post['password'],
				"status"		=> 1
 				);

			if(Auth::attempt($credentials)){
				$json['is_logged_in'] 	= true;
				$json['message']		= "Welcome " . Auth::user()->first_name . "!";
				$json['username']		= Auth::user()->first_name;
			} else {
				$json['is_logged_in']	= false;
				$json['message']		= "Error Logging in.";
			}
		}
		return response()->json($json);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * 
	 * @return Response
	 */
	public function destroy()
	{
		if(Auth::check()){
			Auth::logout();
			$json['is_logged_out'] 	= true;
			$json['message']		= "Successfully Logout.";
		} else {
			$json['is_logged_out'] 	= false;
			$json['message']		= "Error Logging out. Try Again Later.";
		}
		return response()->json($json);
	}

}
