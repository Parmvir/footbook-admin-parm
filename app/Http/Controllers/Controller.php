<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Lookups;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	/**
	 * Get values from lookups table
	 *
	 * @param  string $key
	 * @return array
	 * @author Gat
	 **/
	protected function lookupSelect($key)
	{
		return Lookups::key($key)->lists('value', 'key_id');
	}

}
