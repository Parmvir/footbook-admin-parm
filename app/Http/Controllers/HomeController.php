<?php namespace App\Http\Controllers;

use App\Item;
use App\Theme;
use App\Order;
use App\Product;
use App\ProductProperty;
use Auth;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check()){
			if(Auth::user()->roles->first()->id == 3){ // Industrial
				$items = Item::consumer(2)->get()->groupBy('type_name');
			} else {
				$items = Item::consumer(1)->get()->groupBy('type_name');
			}
		} else {
			$items = Item::consumer(1)->get()->groupBy('type_name');
		}
		$themes = Theme::all();
		$products = Product::all();
		return view('footbook', compact('items', 'themes', 'products'));
	}

	public function admin()
	{
		$this->middleware('auth');
		if(Auth::user()->can('view_admin')){
			$new_order = Order::where('o_status', '=', 'New')->count();
			return view('home',compact('new_order'));
		} else {
			abort(403, 'Unauthorized action. Please contact your Administrator.');
		}
		
	}

	public function industrial()
	{
		$items = Item::all()->groupBy('type_name');
		$themes = Theme::all();

		return view('test', compact('items', 'themes'));
	}
}
