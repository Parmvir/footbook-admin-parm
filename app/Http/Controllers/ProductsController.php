<?php namespace App\Http\Controllers;

use App\Product;
use App\User;
use App\UserInfo;
use App\ProductProperty;
use App\Item;
use App\Theme;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Image;
use DB;
use File;
use Auth;
use App\Order;
use App\OrderItem;

class ProductsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth', ['except' => 'store']);

		if(!Auth::user()->can('view_admin')){
			abort(403, 'Unauthorized action. Please contact your Administrator.');
		}
		
		if(!Auth::user()->can('manage_product')){
			abort(403, 'Unauthorized action. Please contact your Administrator.');
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::paginate(12);

		return view('products.index', compact('products'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->all();

		$product = Product::create([]);

		$path = 'uploads/products/';
		$filename = $product->id . '.' . 'svg';

		// Save to file
		File::put(public_path($path.$filename), $input['slippers']);
		$product->title 		= $input['title'];
		$product->product_path 	= $path.$filename;
		$product->user_id	   	= (Auth::check() ? Auth::user()->id : 0);
		$product->save();


		if (isset($input['properties'])) {
			foreach ($input['properties'] as $key => $value) {
				$product->properties()->save(new ProductProperty([
					'key' => $key,
					'value' => $value
					]));
			}
		}

		if (isset($input['items'])) {
			if(isset($input['items']['accessories'])){
				$accessories = $input['items']['accessories'];
				unset($input['items']['accessories']);

				foreach ($accessories as $key => $value) {
					$input['items']['accessory_'.$value] = $value;
				}
			}
			$product->items()->sync(array_values($input['items']));
		}

		return $product->id;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::find($id)->load(['properties', 'items']);
		$user = User::find($product->user_id);
		return view('products.show', compact('product', 'user'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Product::find($id)->delete();

		return redirect()->route('admin.products.index');
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    	$data['product'] = Product::find($id)->load('properties', 'items')->toArray();

		// Parse properties
    	foreach (array_pull($data['product'], 'properties') as $property) {
    		$data['product']['properties'][$property['key']] = $property['value'];
    	}

		// Parse items
    	foreach (array_pull($data['product'], 'items') as $item) {
    		$data['product']['items'][snake_case(str_replace(' ', '',$item['type_name']))] = $item['id'];
    	}

        // Sizes
    	for($i=6; $i<=12; $i+=0.5) {
    		$data['sizeList'][] = $i;
    	}

        // Items
    	$data['items']['straps'] = Item::whereType('1')->lists('item_id', 'id');
    	$data['items']['print_design'] = Item::whereType('3')->lists('item_id', 'id');

        // Colors
    	$data['colors'] = [
    	'#808285' => 'Grey',
    	'#000000' => 'Black',
    	'#0A4436' => 'Green',
    	'#FD6B35' => 'Orange',
    	'#F6B332' => 'Yellow',
    	'#D0112B' => 'Red'
    	];

    	$data['featured'] = [
    	'1' => 'Yes',
    	'0' => 'No',
    	];

        // $data['themes'] = Theme::lists('name', 'id');

    	return view('products.edit', $data);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\UpdateProductRequest $request)
	{
		$product = Product::find($id)->load('properties', 'items');
		$input = $request->all();

		$product->items()->sync(array_flatten($input['items']));

		// Update properties - non-hack solution
		if (isset($input['properties'])) {
			foreach ($input['properties'] as $key => $value) {
				ProductProperty::updateOrCreate(
					[ 'product_id' => $id, 'key' => $key ],
					[ 'key' => $key, 'value' => $value ]
					);
			}
		}

		$featured = Product::find($id);
		$featured->featured = (int) $input['featured'];
		$featured->save();

		return redirect()->route('admin.products.show', $product);

	}
}
