<?php namespace App\Http\Controllers;

use App\Theme;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;
use Storage;
use Image;
use DB;

class ThemesController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$themes = Theme::paginate(10);

		return view('themes.index', compact('themes'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function search(Request $request)
	{
		$query = $request->input('q');
		$themes = Theme::search($query)->paginate(10);
		$request->flash();

		return view('themes.index', compact('themes', 'query'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['ageDemographicsList'] = $this->lookupSelect('age_demographic');
		$data['ageRangeList'] = $this->lookupSelect('age_range');

		return view('themes.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\StoreThemeRequest $request)
	{
		$input = $request->all();

		// Remove fields if empty
		if (empty($input['age_demographic']))
			array_pull($input, 'age_demographic');
		if (empty($input['age_range']))
			array_pull($input, 'age_range');


		// TODO: Validation
		// Save image
		if ($background = array_pull($input, 'background')) {
			$folder = '/uploads/themes/';
			$filename = $background->getClientOriginalName();
			$path = $folder . $filename;
			$image = Image::make($background)->save(public_path($path) );

			$input['background_path'] = $path;
		}

		$theme = Theme::create($input);


		return redirect()->route('admin.themes.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$theme = Theme::find($id);
		return view('themes.show', compact('theme'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['ageDemographicsList'] = $this->lookupSelect('age_demographic');
		$data['ageRangeList'] = $this->lookupSelect('age_range');
		$data['theme'] = Theme::find($id)->toArray();

		return view('themes.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$theme = Theme::find($id);
		$input = $request->all();

		// Remove fields if empty
		if (empty($input['age_demographic']))
			array_pull($input, 'age_demographic');
		if (empty($input['age_range']))
			array_pull($input, 'age_range');


		// Save image
		if ($background = array_pull($input, 'background')) {
			$folder = '/uploads/themes/';
			$filename = $background->getClientOriginalName();
			$path = $folder . $filename;
			$image = Image::make($background)->save(public_path($path) );

			$input['background_path'] = $path;
		}

		$theme->update($input);


		return redirect()->route('admin.themes.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Theme::find($id)->delete();

		return redirect()->route('admin.themes.index');
	}

}
