<?php namespace App\Http\Controllers;

use App\Item;
use App\ItemProperty;
use App\Order;
use App\OrderItem;
use App\Theme;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Storage;
use Image;
use Auth;

class CheckoutController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');

		if(!Auth::user()->can('view_admin')){
			abort(403, 'Unauthorized action. Please contact your Administrator.');
		}
		
		if(!Auth::user()->can('manage_items')){
			abort(403, 'Unauthorized action. Please contact your Administrator.');
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Order::orderBy('created_at','desc')->paginate(10);
		return view('checkout.index', compact('items'));
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function search(Request $request)
	{
		$query = $request->input('q');
		$items = Order::search($query)->paginate(10);
		$request->flash();

		return view('checkout.index', compact('items', 'query'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['itemTypesList'] = $this->lookupSelect('item_type');
		$data['AvailabilityTypesList'] = $this->lookupSelect('availability');
		$data['GenderAvailabilityTypesList'] = $this->lookupSelect('age_demographic');
		$data['themesList'] = Theme::get()->lists('name', 'id');
		$data['itemThemes'] = null;
		return view('items.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreItemRequest
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->all();
		// dd($input);
		$order 					= new Order;
		$order->order_id 		= Order::generateNewOrderNumber();
		$order->amount 			= (float) $input['grand_total'];
		$order->payment_mode 	= 1;
		$order->ip 				= $request->ip();
		$order->save();

		foreach ($input['order']['item'] as $key => $value) {

			// dd($value['country']);
			$order->items()->save(new OrderItem([
					'product_id' 	=> $input['product_id'],
					'size' 			=> $value['size'],
					'country_size' 	=> $value['country'],
					'quantity' 		=> $value['qty'],
					'amount' 		=> (float) $input['unit_total_price'],
					'total_amount' 	=> (int) $value['qty'] * (float) $input['unit_total_price']
					]));
		}

		return $order->order_id;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$item = Order::find($id)->load(['items']);
		return view('checkout.show', compact('item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		$item = Order::find($id)->load(['items']);
		return view('checkout.edit', compact('item'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$item = Order::with(['items'])->find($id);
		$input = $request->all();

		$item->o_status = $input['o_status'];
		$item->save();

		return redirect()->route('admin.checkout.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Order::find($id)->delete();

		return redirect()->route('admin.checkout.index');
	}

}
