<ul class="personal-gallery">
@forelse ($products as $product)
    <li>
    	<span><a href="#"><img src="{{ asset($product['product_path']) }}" width="150px" height="150px"></a> <h4>Design #{{ $product['id'] }}</h4></span>
  
    	<span><a href="#"><img src="{{ asset($product['product_path']) }}" width="150px" height="150px"></a> <h4>Design #{{ $product['id'] }}</h4></span>
    
    </li>

@empty
  <div class="col-md-12">
      <p class="alert alert-danger">No products found</p>
  </div>
@endforelse
</ul>

<script>
$(function(){
	$('.personal-gallery').owlCarousel({
		autoplay:true,
	    autoplayTimeout:3000,
	    autoplayHoverPause:true,
	    loop:true,
		items: 3,
		navigation: true,
		pagination: false,
		responsive: true,
		controls: true
	});
});
</script>