<div class="row">
    <fieldset>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">Email</label>
      <div class="col-md-4">
      {!! Form::text('email', null, ['class'=>'form-control input-md', 'required']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="password">Password</label>
      <div class="col-md-4">
      {!! Form::input('password', 'password', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="password_confirmation">Confirm Password</label>
      <div class="col-md-4">
      {!! Form::input('password', 'password_confirmation', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Role</label>
      <div class="col-md-4">
            {!! Form::select('role', [''=>'Please Select']+$roles, null, [ 'class' => 'form-control' ] ) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="first_name">First Name</label>
      <div class="col-md-4">
      {!! Form::text('first_name', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="middle_initial">Middle Initial</label>
      <div class="col-md-4">
      {!! Form::text('middle_initial', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="last_name">Last Name</label>
      <div class="col-md-4">
      {!! Form::text('last_name', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- File Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="photo">Photo</label>
      <div class="col-md-4">
        @if (isset($user['photo_path']))
          <img src="{{ asset($user['photo_path']) }}" style="max-width: 100%; margin-bottom: 1em;" />
        @endif
        <input id="photo" name="photo" class="input-file" type="file" />
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="address_1">Address</label>
      <div class="col-md-4">
      {!! Form::text('address_1', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <div class="col-md-4 col-md-offset-4">
      {!! Form::text('address_2', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="contact_num">Contact Number</label>
      <div class="col-md-4">
      {!! Form::text('contact_num', null, ['class'=>'form-control input-md']) !!}
      </div>
    </div>

    <!-- Multiple Radios (inline) -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="status">Status</label>
      <div class="col-md-4">
        <label class="radio-inline">
          {!! Form::radio('status', 1, null) !!}
          Active
        </label>
        <label class="radio-inline">
          {!! Form::radio('status', 0, null) !!}
          Inactive
        </label>
      </div>
    </div>

    </fieldset>

</div>
