<center><h2 class="preview-right-header">Select from <br>previous designs</h2>
<!-- <p><button class="btn btn-green btn-preview-current-design">Current Design</button></p> -->
<ul style="list-style-type: none;">
@foreach($products as $product)


	<li style="text-align: center;"><a href="javascript: void(0);" data-path="{{ $product->product_path }}" class="design-preview"><img src="{{ $product->product_path }}" width="200"><br>{{ $product->id }}</a></li>
@endforeach
</ul>
</center>

<script>
$(function(){
	$(".design-preview").on('click', function(){
		var src = $(this).data('path');
		$(".popup-save-preview-left .center .image-container").html('<img src="'+src+'" width="600" height="600">');
	});

	$(".btn-preview-current-design").on('click', function(){
		$(".popup-save-preview .center .image-container").html($(".main-show-top").eq(0).clone());
	});
});
</script>