<h2>Summary</h2>
<form id="checkout-form">
  {!! Form::token() !!}
  <input type="hidden" name="product_id" id="checkout_product_id" value="{{ $product->id }}">
  <table>
    <tr>
      <th>Item</th>
      <th>Price</th>
    </tr>
    @foreach ($product->items as $item)
    <tr>
      <td>{{ $item->type_name }}</td>
      <td><span>PHP</span> {{ $item->price }}</td>
    </tr>
    @endforeach
  </table>
  <table class="popup-checkout-subtotal">
    <tr>
      <td>Subtotal</td>
      <td><span>PHP</span> {{ $total }}.00
      <input type="hidden" id="unit_total_price" value="{{ $total }}" name="unit_total_price"></td>
    </tr>
  </table>

  <table id="orders_wrapper_counter" class="popup-checkout-grandtotal">
    <tr>
      <td>SIZE</td>
      <td> 
        <select id="item_size">
          <option value="6">6</option>
          <option value="6.5">6.5</option>
          <option value="7">7</option>
          <option value="7.5">7.5</option>
          <option value="8">8</option>
          <option value="8.5">8.5</option>
          <option value="9">9</option>
          <option value="9.5">9.5</option>
          <option value="10">10</option>
          <option value="10.5">10.5</option>
          <option value="11">11</option>
          <option value="11.5">11.5</option>
          <option value="12">12</option>
        </select>
      </td>
      <td>
        <select id="item_size_country">
          <option value="US">US</option>
          <option value="UK">UK</option>
          <option value="EU">EU</option>
        </select>
      </td>
      <td>
        <a href="javascript:(void)" class="btn btn-blue btn-size-legend">Legend</a>
      </td>
      </tr>
    </table>
    <table id="qty-wrapper" style="margin-bottom: 2px;">
        <tr>
              <td style="width:98px;" class="quantity2">QUANTITY </td>

              <td>
                <input type="text" id="item_qty" value="1" style="width:45px; margin-top:6px;">
              </td>
                 
              <td><button type="button" id="add_item_count" class="btn btn-green add-btn">+</button></td>
        
        </tr>
      </table>
      <table>
      <tr>
        <td colspan="3">Total Quantity</td>
        <td colspan="3"><span id="total_qty">0</span></td>
      </tr>
      <tr>
     
        <td colspan="3">Grand Total</td>
        
        <td colspan="3"><span>PHP</span> <span id="grand_total">0.00</span><input type="hidden" name="grand_total" id="grand_total_placeholder"></td>
      </tr>
    
    </table>
</form>
<script>
$(function(){
  var count = 0;
  var count = 0;
  $("#add_item_count").on('click', function(){
    var item_size = $("#item_size").val();
    var item_qty  = parseInt($("#item_qty").val());
    var item_size_country = $("#item_size_country").val();
    var duplicates = false;
    var tmp_ctr = 0;
    // check if there is already an item size for the newly added.
    $(".order_sizes").each(function(){
      if($(this).val() == item_size && $(this).data('country') == item_size_country){
        duplicates = true;
        tmp_ctr = $(this).data("counter");
      }
    });
    if(item_qty > 0 && !duplicates){
      // set ids 
      var wrapper       = "order_wrapper_"+count;
      var item_size_id  = "order_size_"+count;
      var item_qty_id   = "order_qty_"+count;
      var item_qty_id_span = "order_qty_span_"+count;
      // set name
      var item_size_name          = "order[item]["+count+"][size]";
      var item_size_country_name  = "order[item]["+count+"][country]";
      var item_qty_name           = "order[item]["+count+"][qty]";

       var html = "<table id='"+wrapper+"'>"
      html      += "<tr>";
      html      += "<td>SIZE:</td>";
      html      += "<td><input type='hidden' data-counter='"+count+"' data-country='"+item_size_country+"' class='order_sizes' name='"+item_size_name+"' id='"+item_size_id+"' value='"+item_size+"'>"+item_size+"</td><td>" + item_size_country + "<input type='hidden' value='"+item_size_country+"' name='"+item_size_country_name+"'></td>";
      html      += "<td>QTY:</td>";
      html      += "<td><input type='hidden' name='"+item_qty_name+"' class='order_qty' id='"+item_qty_id+"' value='"+item_qty+"'><span id='"+item_qty_id_span+"''>"+item_qty+"</span></td>";
      html      += "<td><button type='button' class='btn btn-red btn-remove' onclick='removeOrderItem("+count+")'>-</button> </td>";
       html      += "</tr>";
       html      += "</table>";


       $(html).insertAfter($("#qty-wrapper"));
      count++;
      $("#item_qty").val(1);
    }else if(item_qty > 0 && duplicates){
      var tmp_item_qty = $("#order_qty_"+tmp_ctr).val();
      var tmp_total = parseInt(tmp_item_qty) + parseInt(item_qty);
      $("#order_qty_"+tmp_ctr).val(tmp_total);
      $("#order_qty_span_"+tmp_ctr).html(tmp_total);
      $("#item_qty").val(1);
    } else {
      alert("Quality must be higher than 0");
    }
    countAndCompute();
  });
});

function countAndCompute(){
  var qty = 0;
  $(".order_qty").each(function(){
    qty += parseInt($(this).val());
  });
  $("#total_qty").html(qty);
  var unit = parseFloat($("#unit_total_price").val());
  var grand_total = qty * unit;
  $("#grand_total").html(addCommas(grand_total.toFixed(2)));
  $("#grand_total_placeholder").val(grand_total.toFixed(2));
}

function addCommas(nStr)  // Add Comma to Price (Numbers)
{
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}


function removeOrderItem(counter){
  $("#order_wrapper_"+counter).remove();
  countAndCompute();
}
</script>