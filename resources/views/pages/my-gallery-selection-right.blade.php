Product to checkout: 
<select id="my_creation" style="width: 200px;">
	<option value="">- Current Design -</option>
	@foreach($products as $product)
		<option value="{{ $product->id }}">Design #{{ $product->id }}</option>
	@endforeach
</select>

<script>
	$(function(){
		$("#my_creation").on('change', function(){
			var id 	= $(this).val();
			if(id == "" || !id){
				slipperDetails = sessionStorage.getItem('slipperDetails');
				slipperDetails = JSON.parse(slipperDetails);
				console.log(slipperDetails);
				$(".popup-checkout-left .center .image-container").html($(".main-show-top").eq(0).clone());
				$.post("/checkout/summary", slipperDetails, function(o){
					$(".summary-details-checkout").html(o);
				});
			} else {
				var src = "uploads/products/" +id+".svg";
				$(".popup-checkout-left .center .image-container").html('<img src="'+src+'" width="600" height="600">');

				$.post("/checkout/summary", {id:id}, function(o){
					$(".summary-details-checkout").html(o);
				});
			}
			
		});
	});
</script>

