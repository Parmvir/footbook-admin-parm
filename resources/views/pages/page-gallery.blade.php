<div class="header">
  <div class="logo"><img src="images/home-logo-unofficial.svg">
   
  </div>
   <h2>Featured Gallery</h2>
  <div class="nav"><a class="btn btn-home">Home</a></div>
</div>
<ul class="gallery-choices js-gallery-choices ">
@forelse ($products as $product)
    <li><a href="#"><img src="{{ asset($product['product_path']) }}"></a> <h4>Design #{{ $product['product_id'] }}<br>By: {{ $product['username'] }}</h4></li>
@empty
  <div class="col-md-12">
      <p class="alert alert-danger"><center><h1>No products found.</h1></center></p>
  </div>
@endforelse
</ul>


<script>
  $(function(){
    $('.js-gallery-choices').owlCarousel({
      items: 2,
      navigation: true,
      pagination: false,
      responsive: true,
      controls: true,
    });


    $(".btn-home,.btn-back").on('click', function(){
      $('.page-home').addClass('active');
      $('.page-home').removeClass('animate');
      $('.btn-green.btn-create-your-own').removeClass('animate');
      $('.page-gallery').hide();
      $('.page-home').show();
    });
  });
</script>