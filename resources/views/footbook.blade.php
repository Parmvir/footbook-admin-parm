<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Footbook</title><link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/vendor.js"></script>
    <script src="js/app.js"></script>
    <script src="js/jquery/jBox.min.js"></script>
    <script src="js/ajaxForm.js"></script>
    <script src="js/jquery-migrate.min.js"></script>
    <script src="js/jquery/inline_validation/jquery.validationEngine.js"></script>
    <script src="js/jquery/inline_validation/jquery.validationEngine-en.js"></script>
    <script src="js/lib/owl.carousel.js"></script>
 



    <link rel="stylesheet" href="js/jquery/jBox.css">
    <link rel="stylesheet" href="js/jquery/inline_validation/validationEngine.jquery.css">
  </head>

  <body class="hide">
    
    <div class="container">
    <div class="page-start">
        <h1><img src="images/home-logo-unofficial.svg"></h1>
        <h2>Create your own flip-flops.</h2>
        <p>Tap anywhere to start</p><a href="javascript: void(0);">&nbsp;</a>
      </div>

      <!-- NO NEED TO UPDATE -->
      <div class="page-choose-theme">

        <div class="header">
          <div class="logo"><img src="images/home-logo-unofficial.svg">
            <h2>Custom Shop</h2>
          </div>
          @if(Auth::check())
          <div class="nav"><a class="btn btn-home">Home</a><!-- <a class="btn btn-cart">Cart</a> -->
            <a href="#">Hi, <span class="username-logged">{{ Auth::user()->first_name }}</span>!</a>
            <!-- <div class="user-logged-in" style="display:none;"> <a href="#" class=".btn .btn-username">Hi, <span class="username-logged">n/a</span>!</a><a href="#" class="btn btn-logout">LOGOUT</a></div> -->
          </div>
          @else
          <div class="nav"><a class="btn btn-home">Home</a><!-- <a class="btn btn-cart">Cart</a> -->
            <!-- <div class="user-logged-in" style="display:none;"> <a href="#" class=".btn .btn-username">Hi, <span class="username-logged">n/a</span>!</a><a href="#" class="btn btn-logout">LOGOUT</a></div> --><a class="btn btn-login">Login</a>
          </div>
          @endif
        </div>
        <h1>Pick a them you want to start from.</h1>
        <ul class="theme-choices">
        @foreach ($themes as $theme)
          <li>
            <div>
              <div @if(!empty($theme->background_path)) style="background-image: url('{{ asset($theme->background_path) }}')" @endif class="image"></div><a data-theme="{{ str_slug($theme->theme_id) }}" class="btn btn-white">{{ $theme->name }}</a>
            </div>
          </li>
        @endforeach
          <!-- <li>
            <div>
              <div style="background-image: url('images/themes/1.jpg')" class="image"></div><a data-theme="theme-summer"  class="grey btn btn-white">City Theme</a>
            </div>
          </li>
          <li>
            <div>
              <div style="background-image: url('images/themes/2.jpg')" class="image"></div><a data-theme="theme-baseball" class="grey btn btn-white">City Theme</a>
            </div>
          </li>
          <li>
            <div>
              <div style="background-image: url('images/themes/3.jpg')" class="image"></div><a data-theme="theme-blue" class="grey btn btn-white">City Theme</a>
            </div>
          </li>  -->
        </ul><a class="btn btn-white bx-next"><span class="glyphicon glyphicon-chevron-right"></span></a><a class="btn btn-white bx-prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      </div>
      <!-- END OF NO NEED TO UPDATE -->
      <div class="page-home">
        <h1><img src="images/home-logo-unofficial.svg"></h1>
        <div class="home-gallery">
          <p>See masterpieces that have been created by others</p><a href="#" class="btn btn-white btn-gallery">Design Gallery</a><br>
          <!-- <a href="#" class="btn btn-white">indust sample</a> -->

        </div>
        <div class="home-create"><img src="images/home-slipper2.svg"><a href="#" class="btn btn-green btn-create-your-own" {{ (Auth::check() ? "" : "style=display:none;" ) }}>Create Your Own</a>
          {!! !Auth::check() ? "<p id='notloggedin'>You're not allowed to create if you are not logged in.</p>" : "" !!}
        </div>
        <div class="home-signup"><img src="images/home-slipper3.svg"><a href="#" {{ (Auth::check() ? "data-name=" . Auth::user()->first_name: "") }} class="btn btn-white btn-signup">{{ (Auth::check() ? "Welcome, ". Auth::user()->first_name . "!" : "Signup / Login") }}</a></div>
      </div>
      <div class="page-gallery">
       <!-- PAGE GALLERY -->
      </div>
      <div class="page-gender-choices">
       <!-- PAGE GENDER CHOICES -->
      <div class="header">
        <div class="logo">
          <img src="images/home-logo-unofficial.svg">
        </div>
        <h2>Select Gender</h2>
        <div class="nav"><a class="btn btn-home home-gender-choices">Home</a></div>
      </div>

        <div class="gender-holder">

          <div class="gender-male"> 
             <img src="images/male.svg">
            <h1 class="male-female-title">MALE</h1>
            <div class="home-gender-male gender"><a href="#" class="btn btn-green btn-create-your-own" data-gender="Male" data-open=".options-wrapper-male" {{ (Auth::check() ? "" : "style=display:none;" ) }}>Adult</a>
              {!! !Auth::check() ? "<p id='notloggedin'>You're not allowed to create if you are not logged in.</p>" : "" !!}
            </div>
             <div class="home-gender-kids gender"><a href="#" class="btn btn-green btn-create-your-own" data-gender="Male-Kids" data-open=".options-wrapper-male-kids" {{ (Auth::check() ? "" : "style=display:none;" ) }}>Kids</a>
              {!! !Auth::check() ? "<p id='notloggedin'>You're not allowed to create if you are not logged in.</p>" : "" !!}
            </div>
          </div>

          <div class="gender-female">
            <img src="images/female.svg">
             <h1 class="male-female-title">FEMALE</h1>
             <div class="home-gender-female gender"><a href="#" class="btn btn-green btn-create-your-own" data-gender="Female" data-open=".options-wrapper-female" {{ (Auth::check() ? "" : "style=display:none;" ) }}>Adult</a>
              {!! !Auth::check() ? "<p id='notloggedin'>You're not allowed to create if you are not logged in.</p>" : "" !!}
            </div>
             <div class="home-gender-kids gender"><a href="#" class="btn btn-green btn-create-your-own" data-gender="Female-Kids" data-open=".options-wrapper-female-kids" {{ (Auth::check() ? "" : "style=display:none;" ) }}>Kids</a>
              {!! !Auth::check() ? "<p id='notloggedin'>You're not allowed to create if you are not logged in.</p>" : "" !!}
            </div>
          </div>
         
         
        </div>
      </div>   <!-- PAGE GENDER CHOICES  end-->

      <div class="page-creator">
        <div class="header">
          <div class="logo"><img src="images/home-logo-unofficial.svg">
            <h2>Custom Shop</h2>
          </div>
          <div class="nav"><a class="btn btn-home">Home</a><!-- <a class="btn btn-cart">Cart</a> -->
            @if(Auth::check())
              <a href="#" class="btn btn-username">Hi, {{ Auth::user()->first_name }}!</a><a href="#" class="btn btn-logout">LOGOUT</a>
            @else
             <a href="#" class="btn btn-username" style="display:none;"></a><a href="#" class="btn btn-logout" style="display:none;">LOGOUT</a><a class="btn btn-login">Login</a>
            @endif
          </div>
        </div>
        <div class="body">
          <div class="options">
            <div class="wrap">
              <input type="hidden" id="product_id">
        <!-- <ul class="options-header" id="size_cat">
                <li><a data-open=".options-size-male" class="">Male</a></li>
                <li><a data-open=".options-size-female" class="">Female</a></li>
                <li><a data-open=".options-size-kids" class="">Kids</a></li>
                <li><a data-open=".options-style">Style</a></li>
                <li><a data-open=".options-design">Custom</a></li>
              </ul> -->

                <div class="options-item options-size options-wrapper-male ops">
                  <div class="newdesign">
                    <div class="clear"></div>
                      <h3>Print Design</h3>

                        <ul id="print_design" class="js-carousel options-design-print-design  newprint">
                          @foreach ($items['Print Design'] as $item)
                            @if($item->gender_availability == 1 || $item->gender_availability == 4)
                              <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                            @endif
                          @endforeach
                        </ul> 
                        <div class="options-design-upload">
                          <a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                          <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
                        </div>
                  
                    <h3>Strap Style</h3>
                    <ul class="js-carousel options-style-strap sole" id="strap_style">
                    @foreach ($items['Strap'] as $item)
                      @if($item->gender_availability == 1 || $item->gender_availability == 4)
                        <li {{ $item->item_id == "men3c" ? "class=active" : "" }}><a class="my-style-strap" data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
                      @endif  
                    @endforeach
                    </ul>
                    <h3>Sole Style</h3>
                    <ul class="js-carousel sole sole-style">
                      <li class="active"><a>Sole 1</a></li>
                      <li><a>Sole 2</a></li>
                      <li><a>Sole 3</a></li>
                    </ul>
                    <ul id="strap_color" style="top: 280px;" class="options-style-color active">
                      <!--li.active: a(data-color="#FFFFFF" data-colorname="white") White-->
                      <!-- <li class="colortest active"><a data-color="#FFFFFF" data-basename="men3c" data-colorname="white">White</a></li> -->
                      <li class="colortest active"><a data-color="#0A4436" data-basename="men3c" data-colorname="green">Green</a></li>
                      <li class="colortest"><a data-color="#808285" data-basename="men3c" data-colorname="grey">Grey</a></li>
                      <li class="colortest"><a data-color="#000000" data-basename="men3c" data-colorname="black">Black</a></li>
                      <li class="colortest"><a data-color="#FD6B35" data-basename="men3c" data-colorname="orange">Orange</a></li>
                      <li class="colortest"><a data-color="#F6B332" data-basename="men3c" data-colorname="yellow">Yellow</a></li>
                      <li class="colortest"><a data-color="#D0112B" data-basename="men3c" data-colorname="red">Red</a></li>
                    </ul>
                    <ul id="sole_color" class="options-style-color">
                      <li class="active"><a data-color="#FFFFFF">White</a></li>
                      <li><a data-color="#808285">Grey</a></li>
                      <li><a data-color="#000000">Black</a></li>
                      <li><a data-color="#0A4436">Green</a></li>
                      <li><a data-color="#FD6B35">Orange</a></li>
                      <li><a data-color="#F6B332">Yellow</a></li>
                      <li><a data-color="#D0112B">Red</a></li>
                    </ul>
                    <div class="clear"></div>
                    <h3>Strap Accessories</h3>
                    <ul id="accessory" class="js-carousel options-strap-accessories newprint">
                    @foreach ($items['Accessory'] as $item)
                      @if($item->gender_availability == 1 || $item->gender_availability == 4)
                        <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                      @endif  
                    @endforeach
                    </ul>
                  </div> <!-- END class newdesign -->
                </div> <!-- END class option-size ops // for male-->


                <div class="options-item options-size options-wrapper-female ops">
                  <div class="newdesign">
                    <div class="clear"></div>
                      <h3>Print Design</h3>

                        <ul id="print_design" class="js-carousel options-design-print-design  newprint">
                          @foreach ($items['Print Design'] as $item)
                            @if($item->gender_availability == 2 || $item->gender_availability == 4)
                              <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                            @endif
                          @endforeach
                        </ul> 
                        <div class="options-design-upload">
                          <a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                          <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
                        </div>
                  
                    <h3>Strap Style</h3>
                    <ul class="js-carousel options-style-strap sole" id="strap_style">
                    @foreach ($items['Strap'] as $item)
                      @if($item->gender_availability == 2 || $item->gender_availability == 4)
                        <li {{ $item->item_id == "men3c" ? "class=active" : "" }}><a class="my-style-strap" data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
                      @endif 
                    @endforeach
                    </ul>
                    <h3>Sole Style</h3>
                    <ul class="js-carousel sole sole-style">
                      <li class="active"><a>Sole 1</a></li>
                      <li><a>Sole 2</a></li>
                      <li><a>Sole 3</a></li>
                    </ul>
                    <ul id="strap_color" style="top: 280px;" class="options-style-color active">
                      <!--li.active: a(data-color="#FFFFFF" data-colorname="white") White-->
                      <!-- <li class="colortest active"><a data-color="#FFFFFF" data-basename="men3c" data-colorname="white">White</a></li> -->
                      <li class="colortest active"><a data-color="#0A4436" data-basename="men3c" data-colorname="green">Green</a></li>
                      <li class="colortest"><a data-color="#808285" data-basename="men3c" data-colorname="grey">Grey</a></li>
                      <li class="colortest"><a data-color="#000000" data-basename="men3c" data-colorname="black">Black</a></li>
                      <li class="colortest"><a data-color="#FD6B35" data-basename="men3c" data-colorname="orange">Orange</a></li>
                      <li class="colortest"><a data-color="#F6B332" data-basename="men3c" data-colorname="yellow">Yellow</a></li>
                      <li class="colortest"><a data-color="#D0112B" data-basename="men3c" data-colorname="red">Red</a></li>
                    </ul>
                    <ul id="sole_color" class="options-style-color">
                      <li class="active"><a data-color="#FFFFFF">White</a></li>
                      <li><a data-color="#808285">Grey</a></li>
                      <li><a data-color="#000000">Black</a></li>
                      <li><a data-color="#0A4436">Green</a></li>
                      <li><a data-color="#FD6B35">Orange</a></li>
                      <li><a data-color="#F6B332">Yellow</a></li>
                      <li><a data-color="#D0112B">Red</a></li>
                    </ul>
                    <div class="clear"></div>
                    <h3>Strap Accessories</h3>
                    <ul id="accessory" class="js-carousel options-strap-accessories newprint">
                    @foreach ($items['Accessory'] as $item)
                      @if($item->gender_availability == 2 || $item->gender_availability == 4)
                        <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                      @endif
                    @endforeach
                    </ul>
                  </div> <!-- END class newdesign -->
                </div> <!-- END class option-size ops // for female-->


                <div class="options-item options-size options-wrapper-male-kids ops">
                  <div class="newdesign">
                    <div class="clear"></div>
                      <h3>Print Design</h3>

                        <ul id="print_design" class="js-carousel options-design-print-design  newprint">
                          @foreach ($items['Print Design'] as $item)
                            @if($item->gender_availability == 3 || $item->gender_availability == 4)
                              <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                            @endif
                          @endforeach
                        </ul> 
                        <div class="options-design-upload">
                          <a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                          <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
                        </div>
                  
                    <h3>Strap Style</h3>
                    <ul class="js-carousel options-style-strap sole" id="strap_style">
                    @foreach ($items['Strap'] as $item)
                      @if($item->gender_availability == 3 || $item->gender_availability == 4)
                        <li {{ $item->item_id == "men3c" ? "class=active" : "" }}><a class="my-style-strap" data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
                      @endif
                    @endforeach
                    </ul>
                    <h3>Sole Style</h3>
                    <ul class="js-carousel sole sole-style">
                      <li class="active"><a>Sole 1</a></li>
                      <li><a>Sole 2</a></li>
                      <li><a>Sole 3</a></li>
                    </ul>
                    <ul id="strap_color" style="top: 280px;" class="options-style-color active">
                      <!--li.active: a(data-color="#FFFFFF" data-colorname="white") White-->
                      <!-- <li class="colortest active"><a data-color="#FFFFFF" data-basename="men3c" data-colorname="white">White</a></li> -->
                      <li class="colortest active"><a data-color="#0A4436" data-basename="men3c" data-colorname="green">Green</a></li>
                      <li class="colortest"><a data-color="#808285" data-basename="men3c" data-colorname="grey">Grey</a></li>
                      <li class="colortest"><a data-color="#000000" data-basename="men3c" data-colorname="black">Black</a></li>
                      <li class="colortest"><a data-color="#FD6B35" data-basename="men3c" data-colorname="orange">Orange</a></li>
                      <li class="colortest"><a data-color="#F6B332" data-basename="men3c" data-colorname="yellow">Yellow</a></li>
                      <li class="colortest"><a data-color="#D0112B" data-basename="men3c" data-colorname="red">Red</a></li>
                    </ul>
                    <ul id="sole_color" class="options-style-color">
                      <li class="active"><a data-color="#FFFFFF">White</a></li>
                      <li><a data-color="#808285">Grey</a></li>
                      <li><a data-color="#000000">Black</a></li>
                      <li><a data-color="#0A4436">Green</a></li>
                      <li><a data-color="#FD6B35">Orange</a></li>
                      <li><a data-color="#F6B332">Yellow</a></li>
                      <li><a data-color="#D0112B">Red</a></li>
                    </ul>
                    <div class="clear"></div>
                    <h3>Strap Accessories</h3>
                    <ul id="accessory" class="js-carousel options-strap-accessories newprint">
                    @foreach ($items['Accessory'] as $item)
                      @if($item->gender_availability == 3 || $item->gender_availability == 4)
                        <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                      @endif
                    @endforeach
                    </ul>
                  </div> <!-- END class newdesign -->
                </div> <!-- END class option-size ops // for Male kids-->

                     <div class="options-item options-size options-wrapper-female-kids ops">
                  <div class="newdesign">
                    <div class="clear"></div>
                      <h3>Print Design</h3>

                        <ul id="print_design" class="js-carousel options-design-print-design  newprint">
                          @foreach ($items['Print Design'] as $item)
                            @if($item->gender_availability == 3 || $item->gender_availability == 4)
                              <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                            @endif
                          @endforeach
                        </ul> 
                        <div class="options-design-upload">
                          <a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                          <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
                        </div>
                  
                    <h3>Strap Style</h3>
                    <ul class="js-carousel options-style-strap sole" id="strap_style">
                    @foreach ($items['Strap'] as $item)
                      @if($item->gender_availability == 3 || $item->gender_availability == 4)
                        <li {{ $item->item_id == "men3c" ? "class=active" : "" }}><a class="my-style-strap" data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
                      @endif
                    @endforeach
                    </ul>
                    <h3>Sole Style</h3>
                    <ul class="js-carousel sole sole-style">
                      <li class="active"><a>Sole 1</a></li>
                      <li><a>Sole 2</a></li>
                      <li><a>Sole 3</a></li>
                    </ul>
                    <ul id="strap_color" style="top: 280px;" class="options-style-color active">
                      <!--li.active: a(data-color="#FFFFFF" data-colorname="white") White-->
                      <!-- <li class="colortest active"><a data-color="#FFFFFF" data-basename="men3c" data-colorname="white">White</a></li> -->
                      <li class="colortest active"><a data-color="#0A4436" data-basename="men3c" data-colorname="green">Green</a></li>
                      <li class="colortest"><a data-color="#808285" data-basename="men3c" data-colorname="grey">Grey</a></li>
                      <li class="colortest"><a data-color="#000000" data-basename="men3c" data-colorname="black">Black</a></li>
                      <li class="colortest"><a data-color="#FD6B35" data-basename="men3c" data-colorname="orange">Orange</a></li>
                      <li class="colortest"><a data-color="#F6B332" data-basename="men3c" data-colorname="yellow">Yellow</a></li>
                      <li class="colortest"><a data-color="#D0112B" data-basename="men3c" data-colorname="red">Red</a></li>
                    </ul>
                    <ul id="sole_color" class="options-style-color">
                      <li class="active"><a data-color="#FFFFFF">White</a></li>
                      <li><a data-color="#808285">Grey</a></li>
                      <li><a data-color="#000000">Black</a></li>
                      <li><a data-color="#0A4436">Green</a></li>
                      <li><a data-color="#FD6B35">Orange</a></li>
                      <li><a data-color="#F6B332">Yellow</a></li>
                      <li><a data-color="#D0112B">Red</a></li>
                    </ul>
                    <div class="clear"></div>
                    <h3>Strap Accessories</h3>
                    <ul id="accessory" class="js-carousel options-strap-accessories newprint">
                    @foreach ($items['Accessory'] as $item)
                      @if($item->gender_availability == 3 || $item->gender_availability == 4)
                        <li><a data-item-id="{{ $item->id }}" data-property="{{ $item->item_id }}" data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                      @endif
                    @endforeach
                    </ul>
                  </div> <!-- END class newdesign -->
                </div> <!-- END class option-size ops // for Female kids-->

            </div> <!-- END class wrap -->
          </div>
          <div class="main">
            <div class="main-place-accessory cover">
              <h2>Drag your accessory into position</h2>
            </div>
            <div class="main-place-accessory">
              <div class="btn-container"><a class="btn btn-red btn-cancel">Cancel</a><a id="accessory_done" class="btn btn-green-light btn-done">Done</a></div>
            </div>
            <div class="main-tools"><!-- <a class="btn btn-orange btn-undo">Undo</a><a class="btn btn-orange btn-redo">Redo</a> -->
              <div class="main-tools-zoom center"><a class="btn btn-orange btn-zoom-out">Zoom out</a>
                <p>100%</p><a class="btn btn-orange btn-zoom-in">Zoom in</a>
              </div>
              <a class="btn btn-green-light btn-save btn-save-preview">Save &amp; Preview</a>
            </div>
            <div class="main-show">
              <div class="main-show-top">
                <!-- Slippers svg container-->
              </div>
              <div class="main-show-left">
                <!-- Slippers svg container-->
                <p>&nbsp;</p>
              </div>
              <div class="main-show-right">
                <!-- Slippers svg container-->
                <p>&nbsp;</p>
              </div>
            </div>
            <div class="main-other">
              <div class="main-view">
                <div class="main-view-top active">
                  <!--include images/preview-top.svg-->
                </div>
                <div class="main-view-left">
                  <!--include images/preview-left.svg-->
                </div>
                <div class="main-view-right"></div>
                <!--.main-view-diagonal
                img(src="images/preview-diagonal.svg")

                -->
              </div>
              <div class="main-checkout">
                <p>PHP <b>0.00</b></p><a class="btn btn-green-light btn-checkout">Checkout</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="popup popup-get-ticket"><a class="btn-close">Close</a><img src="images/modal-signin.svg">
      <p>Your Order Ticket is <span class="order-ticket-number"></span>. Please present it to cashier.</p><a class="btn btn-green-light btn-done">Done</a>
    </div>


    <div class="popup popup-checkout"><a class="btn-close">Close</a>
      <div class="popup-checkout-left" style="height:600px;">

        <div class="center">
          <div class="image-container">
           <!--  <img src="assets/slippers/slippers.svg" width="280"> -->
          </div>
        </div>

      </div>
      <div class="popup-checkout-right">
        <div class="my-creation-list">

        </div>
        <div class="summary-details-checkout">
          <h2>Summary</h2>
          <table>
            <tr>
              <th>Item</th>
              <th>Price</th>
            </tr>
            <tr>
              <td>Flip-flops</td>
              <td><span>PHP</span> 150.00</td>
            </tr>
            <tr>
              <td>Print</td>
              <td><span>PHP</span> 100.00</td>
            </tr>
            <tr>
              <td>Accessory 1 (x1)</td>
              <td><span>PHP</span> 50.00</td>
            </tr>
            <tr>
              <td>Accessory 2 (x1)</td>
              <td><span>PHP</span> 50.00</td>
            </tr>
          </table>
          <table class="popup-checkout-subtotal">
            <tr>
              <td>Subtotal</td>
              <td><span>PHP</span> 350.00</td>
            </tr>
          </table>
          <table class="popup-checkout-grandtotal">
            <tr>
              <td >Quantity</td>
              <td>
                <input type="text" value="1">
              </td>
            </tr>
            <tr>
              <td>Grand Total</td>
              <td><span>PHP</span> 350.00</td>
            </tr>
          </table>
        </div><!-- <a href="javascript:(void)" class="btn btn-blue btn-size-legend">Legend</a> --><a class="btn btn-green-light btn-checkout-final">Checkout</a><!-- <br><div class="center"><a class="btn btn-share">Share it!</a></div> -->
      </div>
    </div>

    <!-- PREVIEW AND SAVE -->
    <div class="popup popup-save-preview"><a class="btn-close">Close</a>
      <div class="popup-save-preview-left" style="height:600px;">
      <div class="preview-header">
          <h2 class="preview-left-header left">YOUR PREVIEW</h2>
          <div class="move-right">
            <button class="left btn btn-green btn-preview-current-design">Current Design</button>
          </div>
      </div>
       <div class="clear"></div>
        <div class="center"><div class="image-container preview"><img src="assets/slippers/slippers.svg" width="280"></div></div>
        <button class="btn btn-green btn-save-design">SAVE THIS DESIGN</button></center>
      </div>
      <div class="popup-save-preview-right">
        <div class="popup-preview-designs"></div>
        <br><center></center>
      </div>
    </div>


    <!-- ACCOUNT DETAILS -->
    <div class="popup popup-account-details"><a class="btn-close">Close</a>
      <div id="popup-account-details-wrapper"></div>
    </div>
    <!-- END ACCOUNT DETAILS -->

    <!-- LOGOUT CONFIRMATION -->
    <div class="popup popup-logout" style="min-height: 100px;"><a class="btn-close">Close</a>
      <div class="center">
        <h2>Are you sure you want to Logout?</h2>
        <button type="button" class="btn btn-yes-logout btn-green-light">Yes</button>
        <button type="button" class="btn btn-no-logout btn-red">No</button>
      </div>
    </div>
    <!-- END LOGOUT -->

    <!-- LOGIN FORM -->
    <form id="login_ajax_form" method="POST" action="{{ route('login.post') }}">

      <div class="popup popup-signin">
        <div style="width: 25%; font-weight: 300; opacity: 0.8">
          <p>Hey there,</p>
          <p>You can login to see you previous works and unfinished designs</p>
        </div>
        
        {!! Form::token() !!}
        <div style="width: 40%; padding-top: 2em;">
          <div style="width: 50%; float: left;">
            <input type="text" name="username" placeholder="Username" class="js-keyboard">
          </div>
          <div style="width: 50%; float: left;">
            <input type="password" id="password" name="password" placeholder="Password" class="js-keyboard">
          </div><!-- <a  href="industrial.html" >Login</a> --><button type="button" style="width: 94%" class="btn btn-small btn-green-light btn-login-popup">Login</button>
        </div>
        
        <div style="width: 30%; padding-top: 2.70em;">
          <p style="margin: 0; padding: 0; font-weight: 300; opacity: 0.8; font-size: 0.9em;">Don't have an account?</p><a style="width: 100%" class="btn btn-small btn-blue btn-register">Register</a>
        </div><a style="cursor: pointer; position: absolute; top: 5px; right: 10px; text-transform: uppercase; opacity: 0.8" class="btn-skip">Skip »</a>
      </div>

    </form>
    <!-- END LOGIN FORM -->
    
    <!-- ACCOUNT DETAILS -->
    <div class="popup popup-account-register"><a class="btn-close">Close</a>
      <form id="form_register_account" method="POST" action="{{route('user.register')}}">
      <h2>Register</h2>
      <div style="float: left; width: 50%;">
          {!! Form::token() !!}
          Email Address * <br>
          <input type="text" class="validate[required,custom[email]]" name="email_address" id="email_address" placeholder="Email Address"><br><span style="color:red; font-style: oblique;" id="email_wrapper"></span><br>
          Password * <br>
          <input type="password" class="validate[required,minSize[6]]" id="reg_password" name="password" placeholder="Password"><br><br>
          Confirm Password * <br>
          <input type="password" class="validate[required,equals[reg_password],minSize[6]]" name="confirm_password" placeholder="Confirm Password"><br><br>
          Type * <br>
          <select class="validate[required]" name="role">
            <option selected>Please Select</option>
            <option value="4">Consumer</option>
            <option value="3">Industrial</option>
          </select>
          <br><br>
          <button type="button" id="btn-register-new-user" class="btn btn-green">REGISTER</button>
      </div>
      <div style="float: right; width: 50%;">
          First Name * <br>
          <input type="text" name="first_name" class="validate[required]" placeholder="First Name"><br><br>
          Middle Initial <br>
          <input type="text" name="middle_initial" placeholder="Middle Initial"><br><br>
          Last Name * <br>
          <input type="text" name="last_name" class="validate[required]" placeholder="Last Name"><br><br>
          Address * <br>
          <input type="text" name="address_1" class="validate[required]" placeholder="Address Line 1"><br>
          <input type="text" name="address_2" placeholder="Address Line 2"><br><br>
          Contact Number <br>
          <input type="text" name="contact_number" placeholder="Contact Number"><br><br>
      </div>
      </form>
    </div>
    <!-- END ACCOUNT DETAILS -->


    <div class="popup popup-accessory">
      <h2>Add this strap accessory?</h2>
      <div><img src="assets/accessory/1.png"></div>
      <p class="currency">+ <span>PHP</span> <strong>50.00</strong></p><a class="btn btn-red js-close-popup">No</a><a class="btn btn-green-light js-add-accessory">Yes</a>
    </div>
  </body>

</html>
<div class="pattern"></div>
<div class="accessory-close"></div>

<script type="text/javascript">
$(function(){
  isLoggedin();
  $("#password").keypress(function(e){
    var key = e.which;
    if(key == 13){
      $(".btn-login-popup").click();
      return false;
    }
  });

  $(".btn-login-popup").on('click', function(){
    var data = $("#login_ajax_form").serializeArray();
    var username = data[1].value;
    var password = data[2].value;
    if(username == "" || password == ""){
      alert("Please enter username / password.");
    } else {
      $.post("{{ route('login.post') }}",data,function(o){
        if(o.is_logged_in){
          alertMe(o.message,'green');
          $(".btn-skip").click();
          $(".btn-login").css({ display: "none" });
          $('.btn-logout, .btn-username').show();
          $(".btn-username").html("Hi, " + o.username + "!");
          $(".btn-signup").html("Welcome, " + o.username + "!");
          $(".btn-signup").attr("data-name", o.username);
          $("body").css('background-color','#c7571d');
          location.reload();
        } else {
          alertMe(o.message,'red');
        }
      }, 'json');
    }
  });

  /* LEGEND */
  $(".btn-size-legend").on('click', function(){
    // alert("test");
    new jBox('Notice', {
      content: "test"
    });
  });
  /* END LEGEND */

  /*REGISTER*/
  var has_error = false;
  $("#email_address").on('focusout', function(){
    var email = $(this).val();
    $.post("{{ route('check.user.email') }}", {email_address: email}, function(o){
      if(o.is_duplicate){
        alertMe(o.message,'red');
        $("#email_wrapper").css({"color": "red"});
        has_error = true;
      } else{
        has_error = false;
        $("#email_wrapper").css({"color": "green"});
      }
      $("#email_wrapper").html(o.message);
    },'json'); 
  });


  $("#form_register_account").validationEngine({scroll:false});
  $('#form_register_account').ajaxForm({
    success:function(o) {
      if(o.is_successful) {
        $("#form_register_account")[0].reset();
        $('.popup-account-register').removeClass('active');
        alertMe(o.message,'green');
      } else {
        alertMe(o.message,'red');
      }
    }, beforeSubmit: function(o) {
    }, dataType : "json"
  });

  $("#btn-register-new-user").on('click', function(){
    if(!has_error){
      $("#form_register_account").submit();
    } else {
      alertMe("We still have error.", 'red');
    }
    
  });

  /*END REGISTER*/

  /* LOGOUT CONFIRMED BUTTON */
  $(".btn-yes-logout").on('click', function(){
    $.post("{{ route('logout.frontend') }}",{},function(o){
      if(o.is_logged_out){
        alertMe(o.message,'green');
        location.reload();
      } else {
        alertMe(o.message,'red');
      }
    },'json');
  });

  $(".home-gender-choices").on('click', function(){
    $('.page-home').removeClass('active');
    $('.page-home').removeClass('animate');
    $('.btn-green.btn-create-your-own').removeClass('animate');
    $('.page-gallery').hide();
    $('.page-gender-choices').hide();
    $('.page-home').addClass('active');
    $('.page-home').show();
  });


  /* LOAD USER VIEW */
  $(".btn-username").on('click', function(){
    $('.popup-account-details').addClass('active');
    loadUserView();
  });

  /* BTN SIGN-UP*/
  $(".btn-signup").on('click', function(){
    if($(".btn-signup").attr('data-name')){
      $('.popup-account-details').addClass('active');
      loadUserView();
    } else {
      $('.popup-signin').addClass('active');
    }
  });
});

function loadUserView(){
  $.post("{{ route('user.popup.view') }}",{}, function(o){
    $("#popup-account-details-wrapper").html(o);
    $("#personal_gallery").html('<img src="{{ asset("css/lib/AjaxLoader.gif") }}">');
    $.post("{{ route('user.popup.view.gallery') }}",{}, function(e){
      $("#personal_gallery").html(e);
    });
  });
}

function alertMe(msg,type){
  new jBox('Notice', {
      content: msg,
      animation: {open: 'tada',close: 'slide:right'},
      color: type,
      audio: 'js/jquery/audio/bling4'
  });
}

function isLoggedin(){
  var isLoggedin = "{{ Auth::check() }}";
  if(isLoggedin){
    setTimeout(function() {
      var $el = $('.transition-to-black').addClass('active');
      setTimeout(function() {
        $('.page-start').hide();
        $el.addClass('unactive');
      }, 500);
      setTimeout(function() {
        $el.removeClass('active unactive');
        $('.transition-to-black').remove();
        $('.page-gallery').hide();
      }, 750);
    }, 200);
    $("body").css('background-color','#c7571d');
  }
}



</script>