@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">ADD ROLE</div>

				<div class="panel-body">

                    @include('errors.list')

                    {!! Form::open([ 'route' => 'admin.roles.store' ]) !!}
						@include('roles.form')
					    <div class="col-xs-12 text-right">
					        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
					    	<a href="{{ route('admin.roles.index') }}" class="btn btn-primary">Cancel</a>
					    </div>
                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
