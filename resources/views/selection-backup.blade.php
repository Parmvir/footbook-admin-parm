  <div class="options-item options-size ops">
    <div class="newdesign">
      <div class="clear"></div>
        <h3>Print Design</h3>

          <ul id="print_design" class="js-carousel options-design-print-design  newprint">

            @foreach ($items['Print Design'] as $item)
              <li><a data-item-id="{{ $item->id }}" data-image="{{ asset($item->texture_path) }}"></a></li>
            @endforeach
  <!--             <li class="active"><a data-item-id="1" data-image="assets/patterns/congruent_pentagon.png"></a></li>
  <li><a data-item-id="1" data-image="assets/patterns/restaurant.png"></a></li>
  <li><a data-item-id="2" data-image="assets/patterns/seamless_paper_texture.png"></a></li>
  <li><a data-item-id="3" data-image="assets/patterns/upfeathers.png"></a></li>
  <li><a data-item-id="4" data-image="assets/patterns/bright_squares.png"></a></li>
  <li><a data-item-id="5" data-image="assets/patterns/green_gobbler.png"></a></li>
  <li><a data-item-id="6" data-image="assets/patterns/skulls.png"></a></li>
  <li><a data-item-id="7" data-image="assets/patterns/stardust.png"></a></li>          -->
          </ul> 



    <div class="options-style">
      <h3>Strap Style</h3>
  <!--  <ul class="js-carousel options-style-strap">
  <li class="active"><a class="my-style-strap" data-basename="men1a"><img src="assets/straps/men1a-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="men3c"><img src="assets/straps/men3c-thumb.png" style="max-height: 90%;"></a></li>
   <li><a class="my-style-strap" data-basename="men2b"><img src="assets/straps/men2b-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="ladies1"><img src="assets/straps/ladies1-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="ladies3"><img src="assets/straps/ladies3-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="ladies4"><img src="assets/straps/ladies4-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="ladies5"><img src="assets/straps/ladies5-thumb.png" style="max-height: 90%;"></a></li>
  <li><a class="my-style-strap" data-basename="ladies6"><img src="assets/straps/ladies6-thumb.png" style="max-height: 90%;"></a></li>
  </ul> -->
      <ul class="js-carousel options-style-strap sole" id="strap_style">
      @foreach ($items['Strap'] as $item)
        <li><a class="my-style-strap" data-item-id="{{ $item->id }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
      @endforeach
      </ul>
  <!-- <ul class="js-carousel options-style-strap sole" id="strap_style">
  @foreach ($items['Strap'] as $item)
    <li><a data-item-id="{{ $item->id }}"><img src="{{ asset($item->texture_path) }}"></a></li>
  @endforeach
  {{--              <li class="active"><a data-item-id="1"><img src="assets/straps/strap-00.svg"></a></li>
  <li><a data-item-id="2"><img src="assets/straps/strap-01.svg"></a></li>
  <li><a data-item-id="3"><img src="assets/straps/strap-02.svg"></a></li>
  <li><a data-item-id="4"><img src="assets/straps/strap-03.svg"></a></li>
  --}}                </ul> -->
    <h3>Sole Style</h3>
      <ul class="js-carousel sole">
        <li class="active"><a>Sole 1</a></li>
  <!--   <li><a>Sole 2</a></li>
  <li><a>Sole 3</a></li> -->
      </ul>
      <ul style="top: 15px;" class="options-style-color active">
        <!--li.active: a(data-color="#FFFFFF" data-colorname="white") White-->
        <li class="colortest active"><a data-color="#0A4436" data-basename="men1a" data-colorname="green">Green</a></li>
        <li class="colortest"><a data-color="#808285" data-basename="men1a" data-colorname="grey">Grey</a></li>
        <li class="colortest"><a data-color="#000000" data-basename="men1a" data-colorname="black">Black</a></li>
        <li class="colortest"><a data-color="#FD6B35" data-basename="men1a" data-colorname="orange">Orange</a></li>
        <li class="colortest"><a data-color="#F6B332" data-basename="men1a" data-colorname="yellow">Yellow</a></li>
        <li class="colortest"><a data-color="#D0112B" data-basename="men1a" data-colorname="red">Red</a></li>
      </ul>
  <!--          <ul id="strap_color" style="top: 50px;" class="options-style-color active">
  <li class="active"><a data-color="#FFFFFF">White</a></li>
  <li><a data-color="#808285">Grey</a></li>
  <li><a data-color="#000000">Black</a></li>
  <li><a data-color="#0A4436">Green</a></li>
  <li><a data-color="#FD6B35">Orange</a></li>
  <li><a data-color="#F6B332">Yellow</a></li>
  <li><a data-color="#D0112B">Red</a></li>
  </ul> -->
      <ul id="sole_color" style="top: 190px;" class="options-style-color">
        <li class="active"><a data-color="#FFFFFF">White</a></li>
        <li><a data-color="#808285">Grey</a></li>
        <li><a data-color="#000000">Black</a></li>
        <li><a data-color="#0A4436">Green</a></li>
        <li><a data-color="#FD6B35">Orange</a></li>
        <li><a data-color="#F6B332">Yellow</a></li>
        <li><a data-color="#D0112B">Red</a></li>
      </ul>
      <div class="clear"></div>
    </div>





    <h3>Strap Accessories</h3>
      <ul class="js-carousel options-strap-accessories newprint">
      @foreach ($items['Accessory'] as $item)
        <li><a data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
      @endforeach
      </ul>
    <!-- {{--                   <li><a data-price="50.00"><img src="assets/accessory/1.png"></a></li>
    <li><a data-price="30.00"><img src="assets/accessory/2.png"></a></li>
    <li><a data-price="20.00"><img src="assets/accessory/3.png"></a></li>
    --}} -->
  







    </div>
  <!--  <li class="active"><a>6</a></li>
  <li><a>6.5</a></li>
  <li><a>7</a></li>
  <li><a>7.5</a></li>
  <li><a>8</a></li>
  <li><a>8.5</a></li>
  <li><a>9</a></li>
  <li><a>9.5</a></li>
  <li><a>10</a></li>
  <li><a>10.5</a></li>
  <li><a>11</a></li>
  <li><a>11.5</a></li>
  <li><a>12</a></li> -->

  </div>

   <!--  <div class="options-item options-style">
                <h3>Strap Style</h3>
                <ul class="js-carousel options-style-strap" id="strap_style">
                  @foreach ($items['Strap'] as $item)
                    <li><a data-item-id="{{ $item->id }}"><img src="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
                </ul>
                <h3>Sole Style</h3>
                <ul class="js-carousel">
                  <li class="active"><a>Sole 1</a></li>
                  <li><a>Sole 2</a></li>
                  <li><a>Sole 3</a></li>
                </ul>
                <ul id="strap_color" style="top: 50px;" class="options-style-color active">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
                <ul id="sole_color" style="top: 190px;" class="options-style-color">
                  <li class="active"><a data-color="#FFFFFF">White</a></li>
                  <li><a data-color="#808285">Grey</a></li>
                  <li><a data-color="#000000">Black</a></li>
                  <li><a data-color="#0A4436">Green</a></li>
                  <li><a data-color="#FD6B35">Orange</a></li>
                  <li><a data-color="#F6B332">Yellow</a></li>
                  <li><a data-color="#D0112B">Red</a></li>
                </ul>
              </div> -->
              <!-- <div class="options-item options-design"> -->
                <!--h3 Strap Texture-->
                <!--ul.js-carousel
                li.active: a Texture 1
                li: a Texture 2
                li: a Texture 3

                -->
                <!-- <h3>Print Design</h3>
                <ul id="print_design" class="js-carousel options-design-print-design">
                  @foreach ($items['Print Design'] as $item)
                    <li><a data-item-id="{{ $item->id }}" data-image="{{ asset($item->texture_path) }}"></a></li>
                  @endforeach
                </ul>
                <div class="options-design-upload"><a class="btn btn-orange btn-small btn-upload">Upload</a>
                  <input type="file" name="image" id="upload" onchange="previewImage(this)" accept="image/*"><a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
                  <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove
                </div>
              </div> -->