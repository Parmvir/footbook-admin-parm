@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">PRODUCT MANAGEMENT</div>

				<div class="panel-body">
                    {!! $products->render() !!}

                    <div class="row">
                        @forelse ($products as $product)
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb" style="margin-bottom: 10px;">
                                <a href="{{ route('admin.products.show', [$product['id']]) }}" class="thumbnail" style="margin-bottom: 10px" href="#">
                                    <img class="img-responsive" src="{{ asset($product['product_path']) }}" alt="">
                                </a>
                                <div class="text-center" style="margin: 10px">
                                <a href="{{ route('admin.products.show', [$product]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="View">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <a href="{{ route('admin.products.edit', [$product]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="Edit">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="{{ route('admin.products.destroy', [$product]) }}" class="btn btn-default btn-xs btn-danger delete-confirm"
                                        data-toggle="tooltip" data-placement="top" title="Delete" data-item="product">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                                </div>
                            </div>
                        @empty
                            <div class="col-md-12">
                                <p class="alert alert-danger">No products found</p>
                            </div>
                        @endforelse
                    </div>

                    {!! $products->render() !!}

                    <div class="pull-right">
                        <a href="{{ route('admin.products.index') }}" class="btn btn-primary">Back</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
