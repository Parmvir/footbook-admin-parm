<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="style.css">
<script src="js/jquery.min.js"></script>
<script src="js/app.js"></script>
<div class="newdesign">
    <div class="clear"></div>
      <h3>Print Design</h3>

        <ul id="print_design" class="js-carousel options-design-print-design  newprint">

          @foreach ($items['Print Design'] as $item)
            <li><a data-item-id="{{ $item->id }}" data-image="{{ asset($item->texture_path) }}"></a></li>
          @endforeach
        </ul> 
        <div class="options-design-upload">
          <a class="btn btn-orange btn-small btn-upload-move-up"><span class="glyphicon glyphicon-arrow-up"></span></a><a class="btn btn-orange btn-small btn-upload-move-down"><span class="glyphicon glyphicon-arrow-down"></span></a><a class="btn btn-orange btn-small btn-upload-move-left"><span class="glyphicon glyphicon-arrow-left"></span></a><a class="btn btn-orange btn-small btn-upload-move-right"><span class="glyphicon glyphicon-arrow-right"></span></a><br><a class="btn btn-orange btn-small btn-upload-zoom-in"><span class="glyphicon glyphicon-zoom-in"></span></a><a class="btn btn-orange btn-small btn-upload-zoom-out"><span class="glyphicon glyphicon-zoom-out"></span></a><a class="btn btn-orange btn-small btn-mirror">Mirror</a>
          <!--a.btn.btn-orange.btn-small.btn-remove-upload Remove-->
        </div>
  <div class="options-style">
    <h3>Strap Style</h3>
    <ul class="js-carousel options-style-strap sole" id="strap_style">
    @foreach ($items['Strap'] as $item)
      <li {{ $item->item_id == "men3c" ? "class=active" : "" }}><a class="my-style-strap" data-item-id="{{ $item->id }}" data-basename="{{ $item->item_id }}"><img src="{{ asset($item->texture_path) }}" style="max-height: 90%;"></a></li>
    @endforeach
    </ul>
  <h3>Sole Style</h3>
    <ul class="js-carousel sole">
      <li class="active"><a>Sole 1</a></li>
      <li><a>Sole 2</a></li>
      <li><a>Sole 3</a></li>
    </ul>
    <ul style="top: 15px;" class="options-style-color active">
      <li class="colortest active"><a data-color="#0A4436" data-basename="men3c" data-colorname="green">Green</a></li>
      <li class="colortest"><a data-color="#808285" data-basename="men3c" data-colorname="grey">Grey</a></li>
      <li class="colortest"><a data-color="#000000" data-basename="men3c" data-colorname="black">Black</a></li>
      <li class="colortest"><a data-color="#FD6B35" data-basename="men3c" data-colorname="orange">Orange</a></li>
      <li class="colortest"><a data-color="#F6B332" data-basename="men3c" data-colorname="yellow">Yellow</a></li>
      <li class="colortest"><a data-color="#D0112B" data-basename="men3c" data-colorname="red">Red</a></li>
    </ul>
    <ul id="sole_color" style="top: 190px;" class="options-style-color">
      <li class="active"><a data-color="#FFFFFF">White</a></li>
      <li><a data-color="#808285">Grey</a></li>
      <li><a data-color="#000000">Black</a></li>
      <li><a data-color="#0A4436">Green</a></li>
      <li><a data-color="#FD6B35">Orange</a></li>
      <li><a data-color="#F6B332">Yellow</a></li>
      <li><a data-color="#D0112B">Red</a></li>
    </ul>
    <div class="clear"></div>
  </div>
  <h3>Strap Accessories</h3>
    <ul class="js-carousel options-strap-accessories newprint">
    @foreach ($items['Accessory'] as $item)
      <li><a data-price="{{ number_format($item->price, 2) }}"><img src="{{ asset($item->texture_path) }}"></a></li>
    @endforeach
    </ul>
  </div>

  <script>
    $(function(){
      $('.js-carousel').owlCarousel({
        items: 4,
        navigation: true,
        pagination: false,
        responsive: false,
      });
    });
  </script>