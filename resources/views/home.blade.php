@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">DASHBOARD</div>
                    <div class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      You have <strong>{!! $new_order !!}</strong> new Order(s)! click <a href="{{ route('admin.checkout.index') }}">here</a> to view.
                    </div>
                <div class="panel-body fb-dashboard">
                    @if(Auth::user()->can('manage_product'))
                    <a href="{{ route('admin.products.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-gallery.png')}}" alt=""/>
                        Product Management
                    </a>
                    @endif

                    @if(Auth::user()->can('manage_orders'))
                    <a href="{{ route('admin.checkout.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-prod.png')}}" alt=""/>
                        Orders Management
                    </a>
                    @endif

                    @if(Auth::user()->can('manage_items'))
                    <a href="{{ route('admin.items.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-prod.png')}}" alt=""/>
                        Item Management
                    </a>
                    @endif
                    <!-- <a href="{{ route('admin.themes.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-theme.png')}}" alt=""/>
                        Theme Management
                    </a> -->

                    @if(Auth::user()->can('manage_users'))
                    <a href="{{ route('admin.users.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-user.png')}}" alt=""/>
                        User Management
                    </a>
                    @endif

                    @if(Auth::user()->can('manage_role_permission') || Auth::user()->can('manage_roles'))
                    <a href="{{ route('admin.roles.index') }}">
                        <img src="{{ asset('/admin-assets/img/icon-role.png')}}" alt=""/>
                        Role Access Management
                    </a>
                    @endif
                </div>
			</div>
		</div>
	</div>
</div>
@endsection
