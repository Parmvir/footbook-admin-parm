@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
            <div class="panel panel-default">
                <div class="panel-heading">ORDER MANAGEMENT</div>
                <div class="panel-body">
                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $items->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $items->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <!-- <a href="{{ route('admin.items.create') }}" class="btn btn-primary">Add Item</a> -->
                        </div>
                    </div>

                    <div id="custom-search-input" class="paging">
                        {!! Form::open([ 'method' => 'GET', 'route' => 'admin.checkout.search' ]) !!}
                        <div class="input-group col-md-12">
                        {!! Form::text('q', null, ['class'=>'form-control input-xs', 'placeholder'=>'Search Order #']) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-xs" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    @if ($items->isEmpty())
                        <p class="alert alert-danger">No items found</p>
                    @else

                    <table class="table table-responsive">
                        <tr>
                            <th>Order ID</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th></th>
                        </tr>

                        @foreach ($items as $item)
                        <tr>
                            <td>{!! $item['order_id'] !!}</td>
                            <td>PHP {!! number_format($item['amount'], 2, '.', ',') !!}</td>
                            <td><span class="label {{ ($item['o_status'] == 'New' ? 'label-success' : ($item['o_status'] == 'Cancelled' ? 'label-danger' : 'label-primary')) }}">{!! $item['o_status'] !!}</span></td>
                            <td class="text-right">
                                <a href="{{ route('admin.checkout.show', [$item]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="View">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                                <a href="{{ route('admin.checkout.edit', [$item]) }}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top" title="Edit">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="{{ route('admin.checkout.destroy', [$item]) }}" class="btn btn-default btn-xs btn-danger delete-confirm"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>

                        @endforeach
                    </table>
                    @endif

                    <div class="row paging">
                        <div class="col-md-6">
                            @if (isset($query))
                                {!! $items->appends(['q'=>$query])->render() !!}
                            @else
                                {!! $items->render() !!}
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <!-- <a href="{{ route('admin.items.create') }}" class="btn btn-primary">Add Item</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
