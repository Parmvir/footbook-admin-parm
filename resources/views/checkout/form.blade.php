<link href="{{ asset('/admin-assets/css/bootstrap-colorpicker.min.css') }}"  rel="stylesheet">
<script src="{{ asset('/admin-assets/js/bootstrap-colorpicker.min.js') }}"></script>
<div class="row">
    <fieldset>

    <!-- Text input-->
    <!-- <div class="form-group">
      <label class="col-md-4 control-label" for="item_id">Item ID</label>
      <div class="col-md-4">
      
      </div>
    </div> -->
    <input type="hidden" name="order_id" value="{{ $item->id }}">
    <!-- SELECT -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Order Status</label>
      <div class="col-md-4">
        {!! Form::select('o_status', [''=>'Please Select','New'=>'New','Processing'=>'Processing','Delivering' => 'Delivering','Done'=>'Done','Cancelled' => 'Cancelled'], null, ['class'=>'form-control', 'required', 'id'=>'o_status']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Order #</label>
      <div class="col-md-4">
        {!! Form::text('order_num', $item['order_id'], ['class'=>'form-control input-md', 'disabled']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Amount</label>
      <div class="col-md-4">
        {!! Form::text('amount', 'PHP ' . number_format($item['amount'],2,'.',',') , ['class'=>'form-control input-md', 'disabled']) !!}      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="type">Payment Mode</label>
      <div class="col-md-4">
        {!! Form::text('payment_mode', ($item['payment_mode'] == 1 ? "CASH" : "CHEQUE"), ['class'=>'form-control input-md', 'disabled']) !!}
      </div>
    </div>
    
    <h4 style="text-align: center;">
      Order Items: 
      @foreach ($item['items'] as $key => $value)
        {{ App\Product::find($value->product_id)->title }}
        <?php break; ?>
      @endforeach
    </h4>
    <center>
      <table class="table table-bordered" style="width: 80%;text-align: center;">
        <thead>
          <th style="text-align: center;">Size</th>
          <th style="text-align: center;">Quantity</th>
          <th style="text-align: center;">Amount</th>
          <th style="text-align: center;">Total Amount</th>
        </thead>
        <tbody>
          @foreach ($item['items'] as $key => $value)
          <tr>
            <td>{{ $value->size }} {{ $value->country_size }}</td>
            <td>{{ $value->quantity }}</td>
            <td>PHP {{ number_format($value->amount,2,'.',',') }}</td>
            <td>PHP {{ number_format($value->total_amount,2,'.',',') }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </center>

    <!-- Text input-->
    <!-- <div class="form-group">
      <label class="col-md-4 control-label" for="background_color">Background Color</label>
      <div class="col-md-4">
        <div class="input-group bgcolor">
        {!! Form::text('background_color', null, ['class'=>'form-control input-md']) !!}
          <span class="input-group-addon"><i style="border: 1px solid #cccccc"></i></span>
        </div>
      </div>
      <script>
          $(function(){
              $('.bgcolor').colorpicker();
          });
      </script>
    </div> -->


    </fieldset>

</div>
