@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">VIEW ITEM</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <h2>{{ $item['type_name'] }}</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Texture:</h4>
                                <p class="text-center">
                                    @foreach ($item['items'] as $key => $value)
                                    <img src="/uploads/products/{{ $value->product_id }}.svg" style="max-width: 100%;">
                                    <?php break; ?>
                                    @endforeach
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h4>Order Status:</h4>
                                <p><span class="label {{ ($item['o_status'] == 'New' ? 'label-success' : 'label-primary') }}">{!! $item['o_status'] !!}</span></p>
                                <h4>Order #:</h4>
                                <p>{!! $item['order_id'] !!}</p>
                                <h4>Amount</h4>
                                <p>PHP {!! number_format($item['amount'],2,'.',',') !!}</p>
                                <h4>Payment Mode:</h4>
                                <p>{!! ($item['payment_mode'] == 1 ? "CASH" : "CHEQUE") !!}</p>
                                <h4>Order Items:</h4>
                                <dl class="dl-horizontal">

                                    @foreach ($item['items'] as $key => $value)
                                    <dt>Product Title: </dt>
                                    <dd>{{ App\Product::find($value->product_id)->title }}</dd>
                                    <?php break; ?>
                                    @endforeach
                                    @foreach ($item['items'] as $key => $value)
                                        <dt>Size: </dt>
                                        <dd>{{ $value->size }} {{ $value->country_size }}</dd>
                                        <dt>Quantity: </dt>
                                        <dd>{{ $value->quantity }}</dd>
                                        <dt>Amount: </dt>
                                        <dd>PHP {{ number_format($value->amount,2,'.',',') }}</dd>
                                        <dt>Total Amount: </dt>
                                        <dd>PHP {{ number_format($value->total_amount,2,'.',',') }}</dd>
                                        <br>
                                    @endforeach
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <a href="{{ route('admin.checkout.edit', [$item['id']]) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.checkout.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection