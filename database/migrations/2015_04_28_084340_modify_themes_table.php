<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyThemesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('themes', function(Blueprint $table)
		{
			$table->string('name')->nullable()->change();
			$table->text('description')->nullable()->change();
			$table->string('gender', 1)->nullable()->change();
			$table->integer('age_demographic')->unsigned()->nullable()->change();
			$table->integer('age_range')->unsigned()->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('themes', function(Blueprint $table)
		{
			$table->string('name')->change();
			$table->string('description')->change();
			$table->string('gender', 1)->change();
			$table->integer('age_demographic')->unsigned()->change();
			$table->integer('age_range')->unsigned()->change();
		});
	}

}
