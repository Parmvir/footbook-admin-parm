
<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolesSeeder extends Seeder {

	public function run()
	{

		// Roles
		$admin = Role::firstOrCreate(['name'=>'Administator']);
		$staff = Role::firstOrCreate(['name'=>'Staff']);
		$industrial = Role::firstOrCreate(['name'=>'Industrial']);
		$consumer = Role::firstOrCreate(['name'=>'Consumer']);

		// Superadmin
		$superAdmin = App\User::firstOrCreate([
			'first_name' => 'Superadm1n',
			'email' => 'john.perez@razerbite.com',
			'password' => bcrypt('demo123')
		])->attachRoles([$admin, $industrial, $staff]);

		// Permissions
		$viewAdmin = Permission::firstOrCreate([
			'name' => 'view_admin',
			'display_name' => 'View admin'
		]);
		$manageUsers = Permission::firstOrCreate([
			'name' => 'manage_users',
			'display_name' => 'Manage users'
		]);
		$bulkOrder = Permission::firstOrCreate([
			'name' => 'bulk_order',
			'display_name' => 'Bulk order'
		]);
		$manageItems = Permission::firstOrCreate([
			'name' => 'manage_items',
			'display_name' => 'Manage items'
		]);
		$approveDesigns = Permission::firstOrCreate([
			'name' => 'approve_designs',
			'display_name' => 'Approve designs'
		]);
		$manageProducts = Permission::firstOrCreate([
			'name' => 'manage_product',
			'display_name' => 'Manage Products'
		]);
		$manageOrders = Permission::firstOrCreate([
			'name' => 'manage_orders',
			'display_name' => 'Manage Orders'
		]);
		$manageRoles = Permission::firstOrCreate([
			'name' => 'manage_roles',
			'display_name' => 'Manage Roles'
		]);
		$manageRolesPermission = Permission::firstOrCreate([
			'name' => 'manage_role_permission',
			'display_name' => 'Manage Role Permissions'
		]);

		// Sync permissions
		$admin->perms()->sync([
			$viewAdmin->id,
			$manageUsers->id,
			$bulkOrder->id,
			$manageItems->id,
			$approveDesigns->id,
			$manageProducts->id,
			$manageOrders->id,
			$manageRoles->id,
			$manageRolesPermission->id,
		]);
		
		$industrial->perms()->sync([
			$bulkOrder->id
		]);

		$staff->perms()->sync([
			$viewAdmin->id,
			$manageItems->id,
			$manageProducts->id,
			$manageOrders->id,
		]);

	}

}
