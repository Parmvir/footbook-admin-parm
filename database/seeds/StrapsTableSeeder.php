<?php

use Illuminate\Database\Seeder;
use App\Item;

class StrapsTableSeeder extends Seeder
{
    public function run()
    {

		Item::create([
			'type' => 1,
			'item_id' => 'pinkfemalekid',
			'texture_path' => '/assets/straps/pinkfemalekid-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'glossy-women',
			'texture_path' => '/assets/straps/glossy-women-top-lightpurple.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'women',
			'texture_path' => '/assets/straps/women-top-pink.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

    }
}
