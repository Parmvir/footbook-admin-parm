<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder {

    public function run()
    {

		// Straps: type = 1
		$straps = [
			"/assets/straps/men1a-thumb.svg",
			"/assets/straps/men2b-thumb.svg",
			"/assets/straps/men3c-thumb.svg",
			"/assets/straps/ladies1-thumb.svg",
			"/assets/straps/ladies2-thumb.svg",
			"/assets/straps/ladies3-thumb.svg"
		];

		Item::create([
			'type' => 1,
			'item_id' => 'men1a',
			'texture_path' => '/assets/straps/men1a-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'men2b',
			'texture_path' => '/assets/straps/men2b-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'men3c',
			'texture_path' => '/assets/straps/men3c-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'ladies1',
			'texture_path' => '/assets/straps/ladies1-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'ladies2',
			'texture_path' => '/assets/straps/ladies2-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		Item::create([
			'type' => 1,
			'item_id' => 'ladies3',
			'texture_path' => '/assets/straps/ladies3-thumb.png',
			'availability' => 3,
			'gender_availability' => 4
		]);

		/*foreach ($straps as $key => $strap) {
			Item::create([
				'type' => 1,
				'item_id' => 'men' . $key,
				'texture_path' => $strap
			]);
		}*/

		// Patterns: type = 3
		$patterns = [
			"/assets/patterns/bright_squares.png",
			"/assets/patterns/congruent_pentagon.png",
			"/assets/patterns/green_gobbler.png",
			"/assets/patterns/restaurant.png",
			"/assets/patterns/seamless_paper_texture.png",
			"/assets/patterns/skulls.png",
			"/assets/patterns/stardust.png",
			"/assets/patterns/upfeathers.png"
		];

		foreach ($patterns as $key => $pattern) {
			Item::create([
				'type' => 3,
				'item_id' => 'print-design-' . $key,
				'texture_path' => $pattern,
				'availability' => 3,
				'gender_availability' => 4
			]);
		}

		// Accessories: type = 4
		$accessories = [
			"/assets/accessory/1.png",
			"/assets/accessory/2.png",
			"/assets/accessory/3.png"
		];

		foreach ($accessories as $key => $accessory) {
			Item::create([
				'type' => 4,
				'item_id' => 'accessory-' . $key,
				'texture_path' => $accessory,
				'availability' => 3,
				'gender_availability' => 4
			]);
		}


    }

}
